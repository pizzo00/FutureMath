//
//  amici_10.swift
//  FutureMath
//
//  Created by Davide on 13/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit


class amici_10: UIViewController {
    @IBOutlet weak var verificaR1: UIButton!
    @IBOutlet weak var verificaR2: UIButton!
    @IBOutlet weak var verificaR3: UIButton!
    @IBOutlet weak var verificaR4: UIButton!
    @IBOutlet weak var verificaR5: UIButton!
    @IBOutlet weak var verificaR6: UIButton!
    
    @IBOutlet weak var generaNew: ZFRippleButton!
    @IBOutlet weak var back: ZFRippleButton!
    @IBOutlet weak var n1random: UILabel!
    @IBOutlet weak var n2random: UILabel!
    @IBOutlet weak var n3random: UILabel!
    @IBOutlet weak var n4random: UILabel!
    @IBOutlet weak var n5random: UILabel!
    @IBOutlet weak var n6random: UILabel!
    @IBOutlet weak var risposta1: UIImageView!
    @IBOutlet weak var risposta2: UIImageView!
    @IBOutlet weak var risposta3: UIImageView!
    @IBOutlet weak var risposta4: UIImageView!
    @IBOutlet weak var risposta5: UIImageView!
    @IBOutlet weak var risposta6: UIImageView!
    @IBOutlet weak var nIns1: UITextField!
    @IBOutlet weak var nIns2: UITextField!
    @IBOutlet weak var nIns3: UITextField!
    @IBOutlet weak var nIns4: UITextField!
    @IBOutlet weak var nIns5: UITextField!
    @IBOutlet weak var nIns6: UITextField!
    
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet var bottoniVerifica: [UIButton]!
    
    @IBOutlet weak var winningLabel: UILabel!
    
    var numGen : [Int] = []

    var b1 = false
    var b2 = false
    var b3 = false
    var b4 = false
    var b5 = false
    var b6 = false
    
    let colors = createColors()
    let confetti = confettiFalling()
    
    var countRisp = 0
    
    private func enabledStyle() {
        
        self.generaNew.isEnabled = true
        self.generaNew.alpha = 1
        
    }
    
    private func disableStyle() {
        
        self.generaNew.isEnabled = false
        self.generaNew.alpha = 0.5
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Score.shared.registerStoryboard(storyboard: self)
    }
    
    private func generaNumeri() {
        
        let n1Random = (arc4random() % 10) + 1
        n1random.text = "\(n1Random)"
        let n2Random = (arc4random() % 10) + 1
        n2random.text = "\(n2Random)"
        let n3Random = (arc4random() % 10) + 1
        n3random.text = "\(n3Random)"
        let n4Random = (arc4random() % 10) + 1
        n4random.text = "\(n4Random)"
        let n5Random = (arc4random() % 10) + 1
        n5random.text = "\(n5Random)"
        let n6Random = (arc4random() % 10) + 1
        n6random.text = "\(n6Random)"
        nIns1.text = ""
        nIns2.text = ""
        nIns3.text = ""
        nIns4.text = ""
        nIns5.text = ""
        nIns6.text = ""
        risposta1.image = nil
        risposta2.image = nil
        risposta3.image = nil
        risposta4.image = nil
        risposta5.image = nil
        risposta6.image = nil
        numGen.removeAll()
        //(sender)
        verificaR1.isEnabled = true
        verificaR2.isEnabled = true
        verificaR3.isEnabled = true
        verificaR4.isEnabled = true
        verificaR5.isEnabled = true
        verificaR6.isEnabled = true
        b1 = false
        b2 = false
        b3 = false
        b4 = false
        b5 = false
        b6 = false
        nIns1.isEnabled = true
        nIns2.isEnabled = true
        nIns3.isEnabled = true
        nIns4.isEnabled = true
        nIns5.isEnabled = true
        nIns6.isEnabled = true
        
    }
    
    
    @IBAction func generaGruppo(_ sender: Any) {
        
        generaNumeri()
        confetti.stopAnimation(view: self.view)
        self.winningLabel.isHidden = true
        countRisp = 0
        
        let n1Random = (arc4random() % 10) + 1
        n1random.text = "\(n1Random)"
        let n2Random = (arc4random() % 10) + 1
        n2random.text = "\(n2Random)"
        let n3Random = (arc4random() % 10) + 1
        n3random.text = "\(n3Random)"
        let n4Random = (arc4random() % 10) + 1
        n4random.text = "\(n4Random)"
        let n5Random = (arc4random() % 10) + 1
        n5random.text = "\(n5Random)"
        let n6Random = (arc4random() % 10) + 1
        n6random.text = "\(n6Random)"
        
        disableStyle()
        
        nIns1.isUserInteractionEnabled = true
        nIns2.isUserInteractionEnabled = true
        nIns3.isUserInteractionEnabled = true
        nIns4.isUserInteractionEnabled = true
        nIns5.isUserInteractionEnabled = true
        nIns6.isUserInteractionEnabled = true
        verificaR1.isUserInteractionEnabled = true
        verificaR1.alpha = 1
        verificaR2.isUserInteractionEnabled = true
        verificaR2.alpha = 1
        verificaR3.isUserInteractionEnabled = true
        verificaR3.alpha = 1
        verificaR4.isUserInteractionEnabled = true
        verificaR4.alpha = 1
        verificaR5.isUserInteractionEnabled = true
        verificaR5.alpha = 1
        verificaR6.isUserInteractionEnabled = true
        verificaR6.alpha = 1
        
        //RISOLUZIONE PROBLEMA NUMERI UGUALI
        let labelCasuali : [UILabel] = [n1random, n2random, n3random, n4random, n5random, n6random]
        var cont = 0
        numGen.append(Int((arc4random() % 10) + 1)) // PRIMO NUMERO CASUALE
        labelCasuali[cont].text = "\(numGen[cont])"
        cont += 1                               // CONTA I NUMERI CASUALI
        while cont < 6 {
            let numCas = (arc4random() % 10) + 1
            var trovato = false
            var numDoppio = 0
            while (!trovato && numDoppio < (numGen.count)) {
                if numCas == numGen[numDoppio]{
                    trovato = true
                }else{
                    numDoppio += 1
                }
            }
            print(numCas)
            if !trovato {
                numGen.append(Int(numCas))
                labelCasuali[cont].text = "\(numGen[cont])"
                cont += 1
            }
        }
        
        //generaNew.isHidden = true
        disableStyle()
        
    }
    // BOTTONI "VERIFICA RISPOSTA"
        @IBAction func verifica1(_ sender: UIButton){
            
            countRisp += 1
            sender.isEnabled = false
            sender.alpha = 0.5
            if let n = Int(nIns1.text!) {
                if (Int(n1random.text!)! + n == 10){
                    risposta1.image = UIImage(named: "corretto.png")
                    verificaR1.isEnabled = false
                    b1 = true;
                    generaNuovi()
                    nIns1.isEnabled = false
                    try! _ = Score.shared.rightAnswer(pointsGained: 1)
                }else{
                    risposta1.image = UIImage(named: "errato.png")
                     wrongLabel()
                    
                    _ = Score.shared.wrongAnswer()
                }
            }else{
                let alert = UIAlertController(title: "ATTENZIONE", message: "Inserisci un valore numerico", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
    }
    @IBAction func verifica2(_ sender: UIButton){
        countRisp += 1
        sender.isEnabled = false
        sender.alpha = 0.5
        if Int(nIns2.text!) != nil {
        if (Int(n2random.text!)! + Int(nIns2.text!)! == 10){
            risposta2.image = UIImage(named: "corretto.png")
            verificaR2.isEnabled = false
            b2 = true
            generaNuovi()
            nIns2.isEnabled = false
            try! _ = Score.shared.rightAnswer(pointsGained: 1)
        }else{
            risposta2.image = UIImage(named: "errato.png")
             wrongLabel()
            
            _ = Score.shared.wrongAnswer()
        }
        }else{
            let alert = UIAlertController(title: "ATTENZIONE", message: "Inserisci un valore numerico", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func verifica3(_ sender: UIButton){
        countRisp += 1
        sender.isEnabled = false
        sender.alpha = 0.5
        if Int(nIns3.text!) != nil {
        if (Int(n3random.text!)! + Int(nIns3.text!)! == 10){
            risposta3.image = UIImage(named: "corretto.png")
            verificaR3.isEnabled = false
            b3 = true
            generaNuovi()
            nIns3.isEnabled = false
            try! _ = Score.shared.rightAnswer(pointsGained: 1)
        }else{
            risposta3.image = UIImage(named: "errato.png")
             wrongLabel()
            
            _ = Score.shared.wrongAnswer()
        }
        }else{ let alert = UIAlertController(title: "ATTENZIONE", message: "Inserisci un valore numerico", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func verifica4(_ sender: UIButton){
        countRisp += 1
        sender.isEnabled = false
        sender.alpha = 0.5
        if Int(nIns4.text!) != nil {
        if (Int(n4random.text!)! + Int(nIns4.text!)! == 10){
            risposta4.image = UIImage(named: "corretto.png")
            verificaR4.isEnabled = false
            b4 = true
            generaNuovi()
            nIns4.isEnabled = false
            try! _ = Score.shared.rightAnswer(pointsGained: 1)
        }else{
            risposta4.image = UIImage(named: "errato.png")
             wrongLabel()
            
            _ = Score.shared.wrongAnswer()
        }
         }else{ let alert = UIAlertController(title: "ATTENZIONE", message: "Inserisci un valore numerico", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func verifica5(_ sender: UIButton){
        countRisp += 1
        sender.isEnabled = false
        sender.alpha = 0.5
        if Int(nIns5.text!) != nil {
        if (Int(n5random.text!)! + Int(nIns5.text!)! == 10){
            risposta5.image = UIImage(named: "corretto.png")
            verificaR5.isEnabled = false
            b5 = true
            generaNuovi()
            nIns5.isEnabled = false
            try! _ = Score.shared.rightAnswer(pointsGained: 1)
        }else{
            risposta5.image = UIImage(named: "errato.png")
             wrongLabel()
            
            _ = Score.shared.wrongAnswer()
        }
        }else{ let alert = UIAlertController(title: "ATTENZIONE", message: "Inserisci un valore numerico", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    @IBAction func verifica6(_ sender: UIButton){
        countRisp += 1
        sender.isEnabled = false
        sender.alpha = 0.5
        if Int(nIns6.text!) != nil {
        if (Int(n6random.text!)! + Int(nIns6.text!)! == 10){
            risposta6.image = UIImage(named: "corretto.png")
            verificaR6.isEnabled = false
            b6 = true
            generaNuovi()
            nIns6.isEnabled = false
            try! _ = Score.shared.rightAnswer(pointsGained: 1)
        }else{
            risposta6.image = UIImage(named: "errato.png")
            wrongLabel()
            
            _ = Score.shared.wrongAnswer()
        }
        }else{let alert = UIAlertController(title: "ATTENZIONE", message: "Inserisci un valore numerico", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func wrongLabel() {
        self.winningLabel.isHidden = false
        self.winningLabel.backgroundColor = colors.red()
        self.winningLabel.text = "Ops, è sbagliato"
        
        for verifica in bottoniVerifica {
            verifica.isEnabled = false
            verifica.alpha = 0.5
        }
        
        self.generaNew.isEnabled = true
        self.generaNew.alpha = 1
        
        
    }
    func generaNuovi()
    {
        print(countRisp)
        if countRisp == 6 {
            
            if (b1 && b2 && b3 && b4 && b5 && b6)
            {
                self.winningLabel.isHidden = false
                confetti.startAnimation(view: self.view)
                
                self.winningLabel.backgroundColor = colors.green()
                self.winningLabel.text = "Bravo, è corretto"
            }
        }
        
    }
    
    @IBAction func back(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        generaNumeri()
        /*verificaR1.isUserInteractionEnabled = false
        verificaR2.isUserInteractionEnabled = false
        verificaR3.isUserInteractionEnabled = false
        verificaR4.isUserInteractionEnabled = false
        verificaR5.isUserInteractionEnabled = false
        verificaR6.isUserInteractionEnabled = false
        nIns1.isUserInteractionEnabled = false
        nIns2.isUserInteractionEnabled = false
        nIns3.isUserInteractionEnabled = false
        nIns4.isUserInteractionEnabled = false
        nIns5.isUserInteractionEnabled = false
        nIns6.isUserInteractionEnabled = false*/
        
        colors.addBackGround(view: self.view)
        textFieldsStyle()
        
        for bottone in bottoniVerifica {
            
            colors.buttonShadows(but: bottone)
        }
        
        colors.buttonShadows(but: self.back)
        colors.buttonShadows(but: self.generaNew)
        
        self.winningLabel.isHidden = true
        
       
    }
    
    private func textFieldsStyle() {
        
        for textField in textFields {
            
            textField.layer.borderWidth = 3
            textField.layer.borderColor = createColors().orange().cgColor
            textField.layer.cornerRadius = 15
            
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    

}

//@IBOutlet var tapButton: UIButton!


