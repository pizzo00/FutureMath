//
//  divisioni_inverse.swift
//  FutureMath
//
//  Created by Studente on 31/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class divisioni_inverse: UIViewController {
    
    @IBOutlet weak var lblFirstNr: UILabel!
    
    @IBOutlet weak var lblDividedNr: UILabel!
    
    @IBOutlet weak var btnTenOutlet: UIButton!
    
    @IBOutlet weak var btnOneHundredOutlet: UIButton!
    
    @IBOutlet weak var btnOneThousandOutlet: UIButton!
    
    @IBOutlet weak var back: UIButton!
    
    @IBOutlet weak var winningLabel: UILabel!
    
    @IBOutlet weak var lblNewRecord: UILabel!
    
    @IBOutlet weak var retry: ZFRippleButton!
    
    @IBOutlet var buttons: [UIButton]!
    
    var factor : Int = 0
    
    var divNumber: Double!
    var mulNumber: Double!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createColors().addBackGround(view: self.view)
        buttonShadows(but: back)
        buttonShadows(but: btnTenOutlet)
        buttonShadows(but: btnOneHundredOutlet)
        buttonShadows(but: btnOneThousandOutlet)
        buttonShadows(but: retry)
        
        startGame()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Score.shared.registerStoryboard(storyboard: self)
    }
    
    private func buttonShadows(but: UIButton) {
        but.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        but.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        but.layer.shadowOpacity = 1.0
        but.layer.shadowRadius = 0.0
        but.layer.masksToBounds = false
    }
    
    func startGame() {
        //Generates a random number
        let number : Int = Int.random(in: 1 ..< 999)
        
        //Divides the initial number randomly by a pow of 10 and displays it
        factor = Int.random(in: 0 ..< 3)
        divNumber = Double(number) / pow(10.0, Double(factor))
        lblFirstNr.text = forTrailingZero(temp: divNumber).replacingOccurrences(of: ".", with: ",")
        
        //Divides the number by 10, 100 or 1000 and displays it
        factor = Int.random(in: 1 ..< 4)
        mulNumber = Double(divNumber) / pow(10.0, Double(factor))
        lblDividedNr.text = forTrailingZero(temp: mulNumber)
    }
    
    func forTrailingZero(temp: Double) -> String {
        return String(format: "%g", temp).replacingOccurrences(of: ".", with: ",")
    }
    
    @IBAction func btnTen(_ sender: UIButton) {
        sender.layer.borderWidth = 5
        
        if factor == 1 {
            won(but: sender)
        }
        else {
            lost(but: sender)
        }
    }
    
    
    @IBAction func btnOneHundred(_ sender: UIButton) {
        sender.layer.borderWidth = 5
        
        if factor == 2 {
            won(but: sender)
        }
        else {
            lost(but: sender)
        }
    }
    
    @IBAction func btnOneThousand(_ sender: UIButton) {
        sender.layer.borderWidth = 5
        
        if factor == 3 {
            won(but: sender)
        }
        else {
            lost(but: sender)
        }
    }
    
    @IBAction func retry(_ sender: UIButton) {
        confettiFalling().stopAnimation(view: self.view)
        
        UIView.animate(withDuration: 0.5) {
            self.lblNewRecord.alpha = 0
        }
        sender.isEnabled = false
        sender.alpha = 0.5
        for but in buttons {
            but.isEnabled = true
            but.layer.borderWidth = 0
            but.alpha = 1
        }

        //starts a new game --> numbers changed
        startGame()
    }
    
    func won(but: UIButton) {
        but.layer.borderColor = createColors().green().cgColor
        for but in buttons {
            but.layer.borderWidth = 0
            but.alpha = 1
        }
        
        self.winningLabel.fadeTransition(0.4)
        self.winningLabel.text = "Ottimo!"
        self.winningLabel.backgroundColor = createColors().green()
        
        //disables the buttons during the animation
        for but in buttons {
            but.isEnabled = false
        }
        UIView.animate(withDuration: 1, delay: 0.4, options: [], animations: {
            self.winningLabel.alpha = 0
        }, completion: { _ in
            self.winningLabel.text = "⇩"
            self.winningLabel.alpha = 1
            self.winningLabel.backgroundColor = nil
            for but in self.buttons {
                but.layer.borderWidth = 0
                but.isEnabled = true
            }
            self.startGame()
        })
        
        do {
            try _ = Score.shared.rightAnswer(pointsGained: 3)
            
        } catch {
            print("error")
        }
    }
    
    func gaveWrongAnswer(but: UIButton) {
        but.layer.borderColor = createColors().red().cgColor
        but.isEnabled = false
        but.alpha = 0.5
        
        self.shakeView(vw: but)
        
        self.winningLabel.fadeTransition(0.4)
        self.winningLabel.text = "Attento!"
        self.winningLabel.backgroundColor = createColors().red()
        
        UIView.animate(withDuration: 1, delay: 0.4, options: [], animations: {
            self.winningLabel.alpha = 0
        }, completion: { _ in
            self.winningLabel.text = "⇩"
            self.winningLabel.alpha = 1
            self.winningLabel.backgroundColor = nil
        })
        
        self.retry.isEnabled = true
        self.retry.alpha = 1
    }
    
    func lost(but: UIButton) {
        let a = Score.shared.wrongAnswer()
        if a.hasLost {
            self.lblNewRecord.backgroundColor = createColors().red()
            UIView.animate(withDuration: 0.5) {
                self.lblNewRecord.alpha = 1
            }
            self.lblNewRecord.text = "Ops, hai perso!"
            self.retry.isEnabled = true
            self.retry.alpha = 1
            for but in buttons {
                but.isEnabled = false
            }
            if a.isNewRecord {
                self.lblNewRecord.backgroundColor = createColors().green()
                self.lblNewRecord.text = "Nuovo record!"
                UIView.animate(withDuration: 0.5) {
                    self.lblNewRecord.alpha = 1
                }
                self.retry.isEnabled = true
                self.retry.alpha = 1
                confettiFalling().startAnimation(view: self.view)
            }
        } else {
            gaveWrongAnswer(but: but)
        }
    }

    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func shakeView(vw: UIButton) {
        let animation = CAKeyframeAnimation()
        animation.keyPath = "position.x"
        animation.values = [0, 10, -10, 10, -5, 5, -5, 0 ]
        animation.keyTimes = [0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1]
        animation.duration = 0.4
        animation.isAdditive = true
        vw.layer.add(animation, forKey: "shake")
    }
}

extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: convertFromCATransitionType(CATransitionType.fade))
    }
}

fileprivate func convertFromCATransitionType(_ input: CATransitionType) -> String {
	return input.rawValue
}
