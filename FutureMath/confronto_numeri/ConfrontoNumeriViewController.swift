//
//  ConfrontoNumeriViewController.swift
//  FutureMath
//
//  Created by Studente on 23/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class ConfrontoNumeriViewController: UIViewController {

    @IBOutlet weak var Numero1: UILabel!
    @IBOutlet weak var Numero2: UILabel!
    @IBOutlet weak var Numero3: UILabel!
    @IBOutlet weak var Numero4: UILabel!
    @IBOutlet weak var Numero5: UILabel!
    @IBOutlet weak var Numero6: UILabel!
    @IBOutlet weak var Numero7: UILabel!
    @IBOutlet weak var Numero8: UILabel!
    @IBOutlet weak var Numero9: UILabel!
    @IBOutlet weak var Numero10: UILabel!
    
    @IBOutlet var numeriSx: [UILabel]!
    @IBOutlet var numeriDx: [UILabel]!
    
    @IBOutlet weak var back: ZFRippleButton!
    
    @IBOutlet weak var btnNuovoEs: UIButton!
    @IBOutlet weak var btnControllo: UIButton!
    @IBOutlet weak var btnConfrontoText_1_2: UIButton!
    @IBOutlet weak var btnConfrontoText_3_4: UIButton!
    @IBOutlet weak var btnConfrontoText_5_6: UIButton!
    @IBOutlet weak var btnConfrontoText_7_8: UIButton!
    @IBOutlet weak var btnConfrontoText_9_10: UIButton!
    var number1 = 0
    var number2 = 0
    var number3 = 0
    var number4 = 0
    var number5 = 0
    var number6 = 0
    var number7 = 0
    var number8 = 0
    var number9 = 0
    var number10 = 0
    var numeri: [Int] = [0,0,0,0,0,0,0,0,0,0]
    
    let confetti = confettiFalling()
    
    func randomNumeri() {
        numeri[0] = Int(arc4random_uniform(99))
        numeri[0] = numeri[0] + 1
        
        numeri[1] = Int(arc4random_uniform(99))
        numeri[1] = numeri[1] + 1
        
        numeri[2] = Int(arc4random_uniform(99))
        numeri[2] = numeri[2] + 1
        
        numeri[3] = Int(arc4random_uniform(99))
        numeri[3] = numeri[3] + 1
        
        numeri[4] = Int(arc4random_uniform(99))
        numeri[4] = numeri[4] + 1
        
        numeri[5] = Int(arc4random_uniform(99))
        numeri[5] = numeri[5] + 1
        
        numeri[6] = Int(arc4random_uniform(99))
        numeri[6] = numeri[6] + 1
        
        numeri[7] = Int(arc4random_uniform(99))
        numeri[7] = numeri[7] + 1
        
        numeri[8] = Int(arc4random_uniform(99))
        numeri[8] = numeri[8] + 1
        
        numeri[9] = Int(arc4random_uniform(99))
        numeri[9] = numeri[9] + 1
        
        var numAX = 0
        numAX = Int(arc4random_uniform(5))
        var provv = numeri[numAX]
        numeri[numAX + 5] = provv
        var numAX2 = Int(arc4random_uniform(5))
        for _ in 0...5
        {
            if numAX2 != numAX
            {

                provv = numeri[numAX2]
                let div = Double(provv) / 10
        
                let myDouble1: Float = Float(div)
                let remainder1 = myDouble1.truncatingRemainder(dividingBy: 1)
                
                var risu = remainder1 * 100
                let rem = provv / 10
                risu = round(risu)
                let provv1 = Int(Int(rem) + Int(risu))
                print(risu)
                print(rem)
                numeri[numAX2 + 5] = provv1

            }
            else
            {
                numAX2 = Int(arc4random_uniform(5))
            }
        }
        
        Numero1.text = String(numeri[0])
        Numero2.text = String(numeri[5])
        Numero3.text = String(numeri[1])
        Numero4.text = String(numeri[6])
        Numero5.text = String(numeri[2])
        Numero6.text = String(numeri[7])
        Numero7.text = String(numeri[3])
        Numero8.text = String(numeri[8])
        Numero9.text = String(numeri[4])
        Numero10.text = String(numeri[9])
    }

    
    @IBAction func back(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnConfronto(_ sender: UIButton) {
        
       // btnControllo.isEnabled = true
        
        restringiESposta(but: sender)
        
        switch sender.currentTitle
        {
            case "=":
                sender.setTitle("<", for: .normal)
            print("avvenuto")
            case "<":
                sender.setTitle(">", for: .normal)
            case ">":
                sender.setTitle("=", for: .normal)
            default:
                sender.setTitle("<", for: .normal)
        }
        if (btnConfrontoText_1_2.currentTitle == "Confronta" || btnConfrontoText_3_4.currentTitle == "Confronta" || btnConfrontoText_5_6.currentTitle == "Confronta" || btnConfrontoText_7_8.currentTitle == "Confronta" || btnConfrontoText_9_10.currentTitle == "Confronta")
        {
            btnControllo.isEnabled = false
        }
        else
        {
            btnControllo.isEnabled = true
        }
        
    }
    
    private func restringiESposta(but: UIButton) {
        
        let center = self.view.center.x - 50
        
        let yPos = but.frame.origin.y
        
        UIView.animate(withDuration: 0.5) {
            but.frame = CGRect(x: center , y: yPos, width: 100, height: 100)
        }
        
    }
    
    private func ripristina(but: UIButton) {
        
        but.setTitle("Confronta", for: .normal)
        
        let center = self.view.center.x - 133
        
        let yPos = but.frame.origin.y
        
        UIView.animate(withDuration: 0.5) {
            but.frame = CGRect(x: center , y: yPos, width: 266, height: 89)
        }
    }
    
    
    
    func Controllo(){
        
        self.btnNuovoEs.isEnabled = true
        //btnControllo.isEnabled = true
        self.btnNuovoEs.alpha = 1
    
        var punteggio = 0
        var perdo = true
        
        let somma1 = numeri[0] - numeri[5]
        let somma2 = numeri[1] - numeri[6]
        let somma3 = numeri[2] - numeri[7]
        let somma4 = numeri[3] - numeri[8]
        let somma5 = numeri[4] - numeri[9]
        print(btnConfrontoText_1_2)
        if (btnConfrontoText_1_2.currentTitle == "<") && (somma1 < 0)
        {
            btnConfrontoText_1_2.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else if(btnConfrontoText_1_2.currentTitle == "=") && (somma1 == 0)
        {
            btnConfrontoText_1_2.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else if(btnConfrontoText_1_2.currentTitle == ">") && (somma1 > 0)
        {
            btnConfrontoText_1_2.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else
        {
            btnConfrontoText_1_2.backgroundColor = createColors().red()
            if perdo
            {
            _ = Score.shared.wrongAnswer()
            print("w")
                perdo = false
            }
        }
        
        if (btnConfrontoText_3_4.currentTitle == "<") && (somma2 < 0)
        {
            btnConfrontoText_3_4.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else if(btnConfrontoText_3_4.currentTitle == "=") && (somma2 == 0)
        {
            btnConfrontoText_3_4.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else if(btnConfrontoText_3_4.currentTitle == ">") && (somma2 > 0)
        {
            btnConfrontoText_3_4.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else
        {
            btnConfrontoText_3_4.backgroundColor = createColors().red()
            if perdo
            {
                _ = Score.shared.wrongAnswer()
                print("w")
                perdo = false
            }

        }
        
        
        
        if (btnConfrontoText_5_6.currentTitle == "<") && (somma3 < 0)
        {
            btnConfrontoText_5_6.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else if(btnConfrontoText_5_6.currentTitle == "=") && (somma3 == 0)
        {
            btnConfrontoText_5_6.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else if(btnConfrontoText_5_6.currentTitle == ">") && (somma3 > 0)
        {
            btnConfrontoText_5_6.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else
        {
            btnConfrontoText_5_6.backgroundColor = createColors().red()
            if perdo
            {
                _ = Score.shared.wrongAnswer()
                print("w")
                perdo = false
            }

        }
        
        
        if (btnConfrontoText_7_8.currentTitle == "<") && (somma4 < 0)
        {
            btnConfrontoText_7_8.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else if(btnConfrontoText_7_8.currentTitle == "=") && (somma4 == 0)
        {
            btnConfrontoText_7_8.backgroundColor = createColors().green()
            punteggio = punteggio + 1
            
        }
        else if(btnConfrontoText_7_8.currentTitle == ">") && (somma4 > 0)
        {
            btnConfrontoText_7_8.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else
        {
            btnConfrontoText_7_8.backgroundColor = createColors().red()
            if perdo
            {
                _ = Score.shared.wrongAnswer()
                print("w")
                perdo = false
            }

        }
        
        
        if (btnConfrontoText_9_10.currentTitle == "<") && (somma5 < 0)
        {
            btnConfrontoText_9_10.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else if(btnConfrontoText_9_10.currentTitle == "=") && (somma5 == 0)
        {
            btnConfrontoText_9_10.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else if(btnConfrontoText_9_10.currentTitle == ">") && (somma5 > 0)
        {
            btnConfrontoText_9_10.backgroundColor = createColors().green()
            punteggio = punteggio + 1
        }
        else
        {
            btnConfrontoText_9_10.backgroundColor = createColors().red()
            if perdo
            {
                _ = Score.shared.wrongAnswer()
                print("w")
                perdo = false
            }

        }
        
        if punteggio == 5
        {
            //btnControllo.isEnabled = true
            do
            {
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            }
            catch
            {
                _ = "?"
            }
            punteggio = 0
            btnControllo.isEnabled = false
            btnNuovoEs.isEnabled = true
            btnNuovoEs.alpha = 1
            confetti.startAnimation(view: self.view)
            
        }
        
    }
    
    
    func coloraBtn()
    {
        btnConfrontoText_1_2.backgroundColor = UIColor(red: 255/255, green: 159/255, blue: 10/255, alpha: 1)
        
        btnConfrontoText_3_4.backgroundColor = UIColor(red: 255/255, green: 159/255, blue: 10/255, alpha: 1)
        
        btnConfrontoText_5_6.backgroundColor = UIColor(red: 255/255, green: 159/255, blue: 10/255, alpha: 1)
        
        btnConfrontoText_7_8.backgroundColor = UIColor(red: 255/255, green: 159/255, blue: 10/255, alpha: 1)
        
        btnConfrontoText_9_10.backgroundColor = UIColor(red: 255/255, green: 159/255, blue: 10/255, alpha: 1)

    }
    
    @IBAction func btnControlla(_ sender: Any) {
        Controllo()
        
    }
    
    @IBAction func btnNuovoEs(_ sender: Any) {
        randomNumeri()
        coloraBtn()
        btnNuovoEs.isEnabled = false
        btnControllo.isEnabled = false
        btnNuovoEs.alpha = 0.5
        
        ripristina(but: self.btnConfrontoText_1_2)
        ripristina(but: self.btnConfrontoText_3_4)
        ripristina(but: self.btnConfrontoText_5_6)
        ripristina(but: self.btnConfrontoText_7_8)
        ripristina(but: self.btnConfrontoText_9_10)
        
        confetti.stopAnimation(view: self.view)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnControllo.isEnabled = false
        let colors = createColors()
        
        colors.addBackGround(view: self.view)
        
        colors.buttonShadows(but: self.btnNuovoEs)
        colors.buttonShadows(but: self.btnControllo)
        colors.buttonShadows(but: self.back)
        colors.buttonShadows(but: self.btnConfrontoText_1_2)
        colors.buttonShadows(but: self.btnConfrontoText_3_4)
        colors.buttonShadows(but: self.btnConfrontoText_5_6)
        colors.buttonShadows(but: self.btnConfrontoText_7_8)
        colors.buttonShadows(but: self.btnConfrontoText_9_10)
        
        btnNuovoEs.isEnabled = false
        btnNuovoEs.alpha = 0.5
        randomNumeri()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Score.shared.registerStoryboard(storyboard: self)
        
        
        
    }
   
}


