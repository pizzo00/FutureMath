//
//  rispostaMultipla.swift
//  FutureMath
//
//  Created by Studente on 16/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class rispostaMultipla: UIViewController {
    
    @IBOutlet weak var rigiocaBtn: UIButton!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var winningLbl: UILabel!
    @IBOutlet var bottoni: [UIButton]!
    @IBOutlet weak var lbl_domanda1: UILabel!
    @IBOutlet weak var lbl_domanda2: UILabel!
    @IBOutlet weak var lbl_domanda3: UILabel!
    @IBOutlet weak var lbl_domanda4: UILabel!
    @IBOutlet weak var lbl_domanda5: UILabel!
    @IBOutlet weak var lbl_domanda6: UILabel!
    @IBOutlet weak var lbl_domanda7: UILabel!
    @IBOutlet weak var lbl_domanda8: UILabel!
    @IBOutlet weak var lbl_domanda9: UILabel!
    @IBOutlet weak var lbl_domanda10: UILabel!
    var num1 = Int()
    var num2 = Int()
    var risultato = Int();
    var domande_Array : [UILabel] = []
    var risposte_Array  : [Int] = []
    var numRnd : [Int] = []
    
    var countRisp = 0
    let confetti = confettiFalling()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        domande_Array = [lbl_domanda1, lbl_domanda2, lbl_domanda3, lbl_domanda4, lbl_domanda5, lbl_domanda6, lbl_domanda7, lbl_domanda8, lbl_domanda9, lbl_domanda10]
        risposte_Array = []
        
        
        randomDomande()
        numRnd = []
        risposteRandom()
        
        disabledStyle()
        
        rigiocaBtn.isEnabled = false
        
        let colors = createColors()
        
        colors.addBackGround(view: self.view)
        
        for bottone in bottoni {
            
            colors.buttonShadows(but: bottone)
        }
        
        colors.buttonShadows(but: rigiocaBtn)
        colors.buttonShadows(but: back)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Score.shared.registerStoryboard(storyboard: self)
    }
    
    private func disabledStyle() {
        
        self.rigiocaBtn.isEnabled = false
        self.rigiocaBtn.alpha = 0.5
        
    }
    
    private func enabledStyle() {
        
        self.rigiocaBtn.isEnabled = true
        self.rigiocaBtn.alpha = 1
    }
    
    func randomDomande()
    {
        for i in 0 ..< domande_Array.count {
            num1 = Int((arc4random() % 10)) + 1
            num2 = Int((arc4random() % 10)) + 1
            domande_Array[i].text = String(num1) + " x " + String(num2)
            risposte_Array.append(num1 * num2)
            print(risposte_Array)
        }
    }
    
    func controlloRisposte(tag1 : Int, num : Int) -> Bool
    {
        
        if (risposte_Array[tag1 / 10] == num)
        {
            return true
        }
        return false
    }
    
    func risposteRandom()
    {
        for i in 0 ..< 10
        {
            
            let rnd = Int(arc4random() % 4) + 1 // posizione del numero esatto
            for btn in self.view.subviews {
        
                if (btn.tag == (i * 10) + 1) || (btn.tag == (i * 10) + 2) || (btn.tag == (i * 10) + 3) || (btn.tag == (i * 10) + 4) {
                    let btn1 = btn as! UIButton
                    
                    let risp_es = (i * 10) + rnd    // tag "base" della riga + rnd
                    
                    // risp_es è il tag del bottone che contiene la risp esatta
                    
                    if btn1.tag == risp_es
                    {
                        btn1.setTitle(String(risposte_Array[i]), for: .normal)
                    }else{
                        var rnd1 = Int((arc4random() % 8)) + 1
                        numRnd.append(rnd1)
                        
                        for i in 0 ..< numRnd.count
                        {
                            if numRnd[i] == rnd1
                            {
                                rnd1 = Int((arc4random() % 8)) + 1
                            }
                        }
                        
                        let rnd2 = Int.random(in: 0...1)
                        if  rnd2 == 0
                        {
                            if (risposte_Array[i] - rnd1) > 0
                            {
                                rnd1 = rnd1 * (-1)
                            }
                        }
                        btn1.setTitle(String(risposte_Array[i] + rnd1) , for: .normal)
                    }
                }
            }
        }
    }
    
    func rigioca()
    {
        risposte_Array.removeAll(keepingCapacity: true)
        randomDomande()
        numRnd = []
        risposteRandom()
        for i in 0..<40{
            bottoni[i].isEnabled = true
            bottoni[i].backgroundColor = createColors().orange()
        }
        confetti.stopAnimation(view: self.view)
        countRisp = 0
        self.winningLbl.isHidden = true
        disabledStyle()
    }
    @IBAction func back(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btn_risp(_ sender: UIButton) {
        let tag = sender.tag
        let num = sender.titleLabel?.text
        
        if (controlloRisposte(tag1: tag, num: Int(num!)!))
        {
            sender.backgroundColor = createColors().green()
            
            countRisp += 1
            
            if (countRisp == 10) {
                
                self.winningLbl.isHidden = false
                self.winningLbl.backgroundColor = createColors().green()
                self.winningLbl.text = "Bravo, è corretto!"
                confetti.startAnimation(view: self.view)
                enabledStyle()
            }
            
            let inizio = (tag / 10) * 4
            
            for j in inizio ..< inizio + 4
            {
                bottoni[j].isEnabled = false
            }
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            } catch {
                print("errore")
            }
        } else {
            sender.backgroundColor = createColors().red()
            
                let inizio = (tag / 10) * 4
            
                for j in inizio ..< inizio + 4
                {
                    bottoni[j].isEnabled = false
                }
            
            
            if Score.shared.wrongAnswer().hasLost {
                self.winningLbl.isHidden = false
                self.winningLbl.text = "Hai perso tutte le vite"
                self.winningLbl.backgroundColor = createColors().red()
                enabledStyle()
                for i in 0..<40
                {
                    bottoni[i].isEnabled = false
                }
                rigiocaBtn.isHidden = false
                rigiocaBtn.isEnabled = true
            } else {
                self.winningLbl.text = "Riprova"
                self.winningLbl.backgroundColor = createColors().red()
                enabledStyle()
            }
        }
    }
    @IBAction func rigioca(_ sender: Any) {
        
        rigioca()
        
    }
}
