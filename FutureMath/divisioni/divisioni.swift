//
//  divisioni2.swift
//  FutureMath
//
//  Created by Luigi  Bisa on 24/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class divisioni: UIViewController {
    
    
    @IBOutlet weak var number1: UILabel!
    @IBOutlet weak var number2: UILabel!
    @IBOutlet weak var number3: UILabel!
    @IBOutlet weak var number4: UILabel!
    @IBOutlet weak var number5: UILabel!
    @IBOutlet weak var result101: UIButton!
    @IBOutlet weak var result102: UIButton!
    @IBOutlet weak var result103: UIButton!
    @IBOutlet weak var result104: UIButton!
    @IBOutlet weak var result105: UIButton!
    @IBOutlet weak var result1001: UIButton!
    @IBOutlet weak var result1002: UIButton!
    @IBOutlet weak var result1003: UIButton!
    @IBOutlet weak var result1004: UIButton!
    @IBOutlet weak var result1005: UIButton!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    
    @IBOutlet var labels: [UILabel]!
    
    
    @IBOutlet weak var controlla: ZFRippleButton!
    @IBOutlet weak var retry: ZFRippleButton!
    @IBOutlet weak var back: ZFRippleButton!
    
    @IBOutlet var tastierino: [UIButton]!
    
    @IBOutlet var inputBoxes: [UIButton]!
    
    
    let colors = createColors()
    
    var chosen : String = ""
    var tag = 0
    var random = [Int]()
    var quo10 : [Int] = []
    var quo100 : [Int] =  []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        randomFill()
        colors.addBackGround(view: self.view)
        colors.buttonShadows(but: self.controlla)
        colors.buttonShadows(but: self.back)
        colors.buttonShadows(but: self.retry)
        
        for numero in tastierino {
            
            colors.buttonShadows(but: numero)
        }
        
        for box in inputBoxes {
            
            colors.buttonShadows(but: box)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Score.shared.registerStoryboard(storyboard: self)
        
    }
    
    /*@IBAction func nButton(_ sender: UIButton) {
        switch chosen{
        case "result101":
            result101.setTitle(String(sender.tag), for: .normal)
        case "result102":
            result102.setTitle(String(sender.tag), for: .normal)
        case "result103":
            result103.setTitle(String(sender.tag), for: .normal)
        case "result104":
            result104.setTitle(String(sender.tag), for: .normal)
        case "result105":
            result105.setTitle(String(sender.tag), for: .normal)
        case "result1001":
            result1001.setTitle(String(sender.tag), for: .normal)
        case "result1002":
            result1002.setTitle(String(sender.tag), for: .normal)
        case "result1003":
            result1003.setTitle(String(sender.tag), for: .normal)
        case "result1004":
            result1004.setTitle(String(sender.tag), for: .normal)
        case "result1005":
            result1005.setTitle(String(sender.tag), for: .normal)
        default:
            chosen = ""
        }
        
    }*/
    
    
    @IBAction func back(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    var numberSelected = 0
    
    //var arrayLabels: [UILabel] = [self.number1;self.number2,self.number3,self.number4,self.number5 ]
    
    @IBAction func selectResult(_ sender: UIButton) {
        
        sender.backgroundColor = .red
        
        clearTag()
        switch sender.tag {
        case 1:
            chosen = "result101"
            break
        case 2:
            chosen = "result102"
        case 3:
            chosen = "result103"
        case 4:
            chosen = "result104"
        case 5:
            chosen = "result105"
        case 6:
            chosen = "result1001"
        case 7:
            chosen = "result1002"
        case 8:
            chosen = "result1003"
        case 9:
            chosen = "result1004"
        case 10:
            chosen = "result1005"
        default:
            chosen = ""
        }
        
        print(chosen)
    }
    
    func clearTag() {
        tag = 0
    }
    
    @IBAction func checkButton(_ sender: Any) {
        
        div10()
        div100()
        check()
    }
    
    func showNumb(){
        number1.text = String(random[0])
        number2.text = String(random[1])
        number3.text = String(random[2])
        number4.text = String(random[3])
        number5.text = String(random[4])
    }
    
    func randomFill(){
        for label in self.labels{
            
            label.text = String(generate())
        }
    }
    
    private func generate() -> Int {
        
        return Int((arc4random() % 900) + 100)
        
    }
    
    func div10(){
        for i in 0...5{
            quo10[i] = random[i] / 10
        }
        
    }
    func div100(){
        for i in 0...5{
            quo100[i] = Int((arc4random() % 900) + 100)
        }
    }
    
    func check(){
        if (result101!.currentTitle == String(quo10[0])){
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            }
            catch{}
        }
        else{
            _ = Score.shared.wrongAnswer()
        }
        if (result102!.currentTitle == String(quo10[1])){
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            }
            catch{}
        }
        else{
            _ = Score.shared.wrongAnswer()
        }
        if (result103!.currentTitle == String(quo10[2])){
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            }
            catch{}
        }
        else{
            _ = Score.shared.wrongAnswer()
        }
        if (result104!.currentTitle == String(quo10[3])){
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            }
            catch{}
        }
        else{
            _ = Score.shared.wrongAnswer()
        }
        if (result105!.currentTitle == String(quo10[4])){
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            }
            catch{}
        }
        else{
            _ = Score.shared.wrongAnswer()
        }
        if (result1001!.currentTitle == String(quo100[0])){
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            }
            catch{}
        }
        else{
            _ = Score.shared.wrongAnswer()
        }
        
        if (result1002!.currentTitle == String(quo100[1])){
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            }
            catch{}
        }
        else{
            _ = Score.shared.wrongAnswer()
        }
        if (result1003!.currentTitle == String(quo100[2])){
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            }
            catch{}
        }
        else{
            _ = Score.shared.wrongAnswer()
        }
        if (result1004!.currentTitle == String(quo100[3])){
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            }
            catch{}
        }
        else{
            _ = Score.shared.wrongAnswer()
        }
        if (result1005!.currentTitle == String(quo100[4])){
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            }
            catch{}
        }
        else{
            _ = Score.shared.wrongAnswer()
        }
    }
    
    @IBAction func keyboardAction(_ sender: UIButton) {
        
        
        switch chosen {
        case "result101":
            let title101 = result101.titleLabel?.text ?? ""
            result101.setTitle(title101 + sender.currentTitle!, for: UIControl.State.normal)
            break
        case "result102":
            let title102 = result102.titleLabel?.text ?? ""
            result102.setTitle(title102 + sender.currentTitle!, for: UIControl.State.normal)
        case "result103":
            let title103 = result103.titleLabel?.text ?? ""
            result103.setTitle(title103 + sender.currentTitle!, for: UIControl.State.normal)
        case "result104":
            let title104 = result104.titleLabel?.text ?? ""
            result104.setTitle(title104 + sender.currentTitle!, for: UIControl.State.normal)
        case "result105":
            let title105 = result105.titleLabel?.text ?? ""
            result105.setTitle(title105 + sender.currentTitle!, for: UIControl.State.normal)
        case "result1001":
            let title1001 = result1001.titleLabel?.text ?? ""
            result1001.setTitle(title1001 + sender.currentTitle!, for: UIControl.State.normal)
        case "result1002":
            let title1002 = result1002.titleLabel?.text ?? ""
            result1002.setTitle(title1002 + sender.currentTitle!, for: UIControl.State.normal)
        case "result1003":
            let title1003 = result1003.titleLabel?.text ?? ""
            result1003.setTitle(title1003 + sender.currentTitle!, for: UIControl.State.normal)
        case "result1004":
            let title1004 = result1004.titleLabel?.text ?? ""
            result1004.setTitle(title1004 + sender.currentTitle!, for: UIControl.State.normal)
        case "result1005":
            let title1005 = result1005.titleLabel?.text ?? ""
            result1005.setTitle(title1005 + sender.currentTitle!, for: UIControl.State.normal)
        default:
            break
        }
        
    }
    
    
    
    
}
