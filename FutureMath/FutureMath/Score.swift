//
//  Score.swift
//  FutureMath
//
//  Created by Davide on 24/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class Score
{
    static private var score : Score? = nil
    static var shared : Score
    {
        get
        {
            if(score == nil)
            {
                score = Score()
            }
            return score!
        }
    }
    
    private(set) var Lifes : Int
    private(set) var Points : Int
    private(set) var PointsRecord : Int
    
    private var storyboard: ScoreStoryboard?
    
    /**
     This Class is Singleton please use the static reference Score.shared
     */
    private init()
    {
        self.Lifes = 3
        self.Points = 0
        self.PointsRecord = UserDefaults.standard.integer(forKey: "pointsRecord")
        self.storyboard = nil
    }
    
    /**
     Draw the score board to your storyboard and save it to update the score.
     
     - Parameter storyboard: The storyboard.
     */
    func registerStoryboard(storyboard: UIViewController)
    {
        self.storyboard = ScoreStoryboard(storyboard: storyboard)
        
        self.storyboard!.writeLifes(nLifes: self.Lifes)
        self.storyboard!.writeScore(newScore: self.Points)
        self.storyboard!.writeRecord(newRecord: self.PointsRecord)
    }
    
    
    /**
     Adds the points gained with a right answer.
     
     Calling this method and increases `points` by the value of `pointsGained`.
     
     - Parameter pointsGained: The points to add.
     - Precondition: `pointsGained` must be greater than 0.
     - Throws: `ScoreError.NegativePointsGain`
                if `pointsGained` is not grater than 0.
     - Returns: The number of point after the addition.
     */
    func rightAnswer(pointsGained: Int) throws -> Int
    {
        if(pointsGained <= 0)
        {
            throw ScoreError.NegativePointsGain
        }
        self.Points += pointsGained
        self.storyboard!.writeScore(newScore: self.Points)
        
        return self.Points
    }
    
    /**
     Call this if the user give a wrong answer.
     
     Calling this method decrement `life` by 1.
     When the number of lifes become 0 this method also reset the points and in case set the new record.
     
     - Returns:
     - lifeRemaining: The number of lifes remaining.
     - hasLost: True if the user lost all lifes.
     - isNewRecord: True if the user set a new record.
     - Postcondition: If the game was lost the number of lifes returned will be the initial number of lifes because it is resetted
     */
    func wrongAnswer() -> (lifeRemaining: Int, hasLost: Bool, isNewRecord: Bool)
    {
        self.Lifes -= 1
        let hasLost = self.Lifes == 0
        var isNewRecord = false
        if(hasLost) // Lost last life
        {
            isNewRecord = lost()
        }
        
        self.storyboard!.writeLifes(nLifes: self.Lifes)
        return (self.Lifes, hasLost, isNewRecord)
    }
    
    private func lost() -> Bool
    {
        let isNewRecord = self.Points > self.PointsRecord
        if(isNewRecord)
        {
            self.PointsRecord = self.Points
            UserDefaults.standard.set(self.PointsRecord, forKey: "pointsRecord")
            self.storyboard!.writeRecord(newRecord: self.PointsRecord)
        }
        self.Lifes = 3
        self.Points = 0
        self.storyboard!.writeLifes(nLifes: self.Lifes)
        self.storyboard!.writeScore(newScore: self.Points)
        return isNewRecord
    }
    
    enum ScoreError: Error
    {
        case NegativePointsGain
    }
}

private class ScoreStoryboard
{
    public var Storyboard: UIViewController
    
    private var lbl_score: UILabel
    private var lbl_lifes: [UIButton]
    private var lbl_record: UILabel
    
	/**
     This class allows you to add and keep update the score dashboard to a viewcontroller
    */
    init(storyboard: UIViewController)
    {
        self.Storyboard = storyboard
        lbl_score = UILabel()
        lbl_lifes = [UIButton(), UIButton(), UIButton()]
        lbl_record = UILabel()
        draw()
    }
    
	/**
	 This method adds the score dashboard to the given ViewController 
    */
	private func draw()
    {
        let titleWidth = 120
        let titleHeight = 40

        let valueWidth = 120
        let valueHeight = 39
        
        let lifeWidth = 50
        let lifeHeight = 50
        
        let center = Storyboard.view.center.x
        
        //--------LABEL--------//
        let lbl_titleScore = UILabel(frame: CGRect(x: 0, y: 0, width: titleWidth, height: titleHeight))        
        let lbl_titleLifes = UILabel(frame: CGRect(x: 0, y: 0, width: titleWidth, height: titleHeight))        
        let lbl_titleRecord = UILabel(frame: CGRect(x: 0, y: 0, width: titleWidth, height: titleHeight))
        self.lbl_score = UILabel(frame: CGRect(x: 0, y: 0, width: valueWidth, height: valueHeight))
        self.lbl_record = UILabel(frame: CGRect(x: 0, y: 0, width: valueWidth, height: valueHeight))
		
        labelsStyle(label: lbl_titleScore)
        labelsStyle(label: lbl_titleLifes)
        labelsStyle(label: lbl_titleRecord)        
        labelsStyle(label: lbl_score)
        labelsStyle(label: lbl_record)
        
        lbl_titleScore.center = CGPoint(x: center - 240, y: 80)
        lbl_titleLifes.center = CGPoint(x: center, y: 80)
        lbl_titleRecord.center = CGPoint(x: center + 240, y: 80)
        self.lbl_score.center = CGPoint(x: center - 240, y: 129)
        self.lbl_record.center = CGPoint(x: center + 240, y: 129)
        
        lbl_titleScore.text = "Score"
        lbl_titleLifes.text = "Lifes"
        lbl_titleRecord.text = "Record"
        self.lbl_score.text = "0"
        self.lbl_record.text = "0"
        
        //--------LIFES--------//
        
        self.lbl_lifes[0] = UIButton(frame: CGRect(x: 0, y: 0, width: lifeWidth, height: lifeHeight))
        
        self.lbl_lifes[1] = UIButton(frame: CGRect(x: 0, y: 0, width: lifeWidth, height: lifeHeight))
       
        self.lbl_lifes[2] = UIButton(frame: CGRect(x: 0, y: 0, width: lifeWidth, height: lifeHeight))
        
        
        
        
        self.lbl_lifes[0].center = CGPoint(x: center - 56, y: 129)
        self.lbl_lifes[1].center = CGPoint(x: center, y: 129)
        self.lbl_lifes[2].center = CGPoint(x: center + 56, y: 129)
        
        self.lbl_lifes[0].setTitle("❤️", for: .normal)
        self.lbl_lifes[1].setTitle("❤️", for: .normal)
        self.lbl_lifes[2].setTitle("❤️", for: .normal)
        
        heartBeatAnim(but: self.lbl_lifes[0])
        heartBeatAnim(but: self.lbl_lifes[1])
        heartBeatAnim(but: self.lbl_lifes[2])
        
        fontDimension(but: self.lbl_lifes[0])
        fontDimension(but: self.lbl_lifes[1])
        fontDimension(but: self.lbl_lifes[2])
        
        lbl_titleScore.textAlignment = .center
        lbl_titleLifes.textAlignment = .center
        lbl_titleRecord.textAlignment = .center
        self.lbl_score.textAlignment = .center
        self.lbl_record.textAlignment = .center
        
        
        self.Storyboard.view.addSubview(lbl_titleScore)
        self.Storyboard.view.addSubview(lbl_titleLifes)
        self.Storyboard.view.addSubview(lbl_titleRecord)
        self.Storyboard.view.addSubview(self.lbl_score)
        self.Storyboard.view.addSubview(self.lbl_record)
        self.Storyboard.view.addSubview(self.lbl_lifes[0])
        self.Storyboard.view.addSubview(self.lbl_lifes[1])
        self.Storyboard.view.addSubview(self.lbl_lifes[2])
    }
    
    private func heartBeatAnim(but: UIButton) {
            
        UIView.animate(withDuration: 1, delay: 0.0, options:[UIView.AnimationOptions.repeat, UIView.AnimationOptions.autoreverse, .allowUserInteraction], animations: {
                but.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
                but.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: nil)
            
    }
    
    
    private func labelsStyle(label: UILabel) {
        
        label.font = UIFont(name: "Futurama Bold Font", size: 30)
        label.textColor = .white   
    }
    
    private func fontDimension(but: UIButton) {
        
        but.titleLabel!.font = UIFont(name: "Futurama Bold Font", size: 35)
      
        
    }
    
    //Update score label text
    func writeScore(newScore: Int)
    {
        self.lbl_score.text = String(newScore)
    }
    
	/**
	 Update record label text
	
	 - Parameter newRecord: The record to show.
    */
    func writeRecord(newRecord: Int)
    {
        self.lbl_record.text = String(newRecord)
    }
    
	/**
	 Update lifes labels
	
	 - Parameter nLifes: The lifes to show.
    */
    func writeLifes(nLifes: Int)
    {
		//Hide all life and then show only the one active
        self.lbl_lifes[0].isHidden = true
        self.lbl_lifes[1].isHidden = true
        self.lbl_lifes[2].isHidden = true
        
        for i in 0...nLifes-1 //nLifes is 1-based
        {
            self.lbl_lifes[i].isHidden = false
        }
    }
}
