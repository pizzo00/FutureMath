//
//  InitialViewController.swift
//  FutureMath
//
//  Created by Alberto Battiston on 06/11/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

enum Images {
    
    static let star1 = UIImage(named: "star1")
    static let star2 = UIImage(named: "star2")
}

class InitialViewController: UIViewController, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var rocket: UIImageView!
    @IBOutlet weak var play: ZFRippleButton!
    @IBOutlet weak var smokeView: UIImageView!
    
    let color = createColors()
    var emitter1 = CAEmitterLayer()
    var emitter2 = CAEmitterLayer()
    var emitter3 = CAEmitterLayer()
    var emitter4 = CAEmitterLayer()
    
    
    
    var rocketPos: CGPoint?
    
    let transition = CircularTransition()
    
    var images:[UIImage] = [Images.star1!, Images.star2!]
    
    var velocities:[Int] = [ 100, 200, 70, 40]
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        return .lightContent
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        rocketPos = rocket.center
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        generateEmitter(emitter: emitter1, originPoint: CGPoint(x:self.view.frame.width/2, y: 200))
        
        self.pulseButtonAnimation()
        
    }
    
    func generateEmitter(emitter: CAEmitterLayer, originPoint: CGPoint) {
        
        emitter.emitterPosition = originPoint
        emitter.emitterShape = CAEmitterLayerEmitterShape.circle
        emitter.emitterSize = CGSize(width: self.view.frame.size.width, height: 2.0)
        emitter.emitterCells = generateEmitterCells()
        self.view.layer.addSublayer(emitter)
        
    }
    
    func shakeView(vw: UIView) {
        let animation = CAKeyframeAnimation()
        animation.keyPath = "position.x"
        animation.values = [0, 10, -10, 10, -5, 5, -5, 0 ]
        animation.keyTimes = [0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1]
        animation.duration = 0.4
        animation.isAdditive = true
        
        rocket.layer.add(animation, forKey: "shake")
    }
    
    private func generateEmitterCells() -> [CAEmitterCell] {
        var cells:[CAEmitterCell] = [CAEmitterCell]()
        for index in 0..<4 {
            
            let cell = CAEmitterCell()
            
            cell.birthRate = 2.0
            cell.lifetime = 30.0
            cell.lifetimeRange = 1
            cell.velocity = CGFloat(getRandomVelocity())
            cell.velocityRange = 0
            cell.emissionLongitude = CGFloat(Double.pi / 2)
            cell.emissionRange = 0.5
            cell.spin = 3.5
            cell.spinRange = 0
            
            
            cell.contents = getNextImage(i: index)
            cell.scaleRange = 0.25
            cell.scale = 0.1
            
            cells.append(cell)
            
        }
        
        return cells
        
    }
    
    private func getRandomVelocity() -> Int {
        return velocities[getRandomNumber()]
    }
    
    private func getRandomNumber() -> Int {
        return Int(arc4random_uniform(4))
    }
    
    
    private func getNextImage(i:Int) -> CGImage {
        return images[i % 2].cgImage!
    }
    
    @IBAction func pressToStart(_ sender: UIButton) {
        
        self.shakeView(vw: rocket)
        
        UIView.animate(withDuration: 1.5, delay: 0.3, options: [], animations: {
            self.rocket.center.y = -200
        }, completion: nil)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "viewController") as! ViewController
        newViewController.transitioningDelegate = self
        newViewController.modalPresentationStyle = .custom
        self.present(newViewController, animated: true, completion: nil)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let second = segue.destination as! ViewController
        second.transitioningDelegate = self
        second.modalPresentationStyle = .custom
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.startingPoint = rocketPos!
        transition.circleColor = color.hexStringToUIColor(hex: "F9C437")
        return transition
    }
    
    private func pulseButtonAnimation() {
        UIView.animate(withDuration: 1, delay: 0.0, options:[UIView.AnimationOptions.repeat, UIView.AnimationOptions.autoreverse, .allowUserInteraction], animations: {
            self.play.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
            self.play.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: nil)
        
    }
    
    
    
    

}
