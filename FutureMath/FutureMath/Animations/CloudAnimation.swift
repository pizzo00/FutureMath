//
//  CloudAnimation.swift
//  FutureMath
//
//  Created by Alberto Battiston on 19/11/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

enum Clouds {
    
    static let cloud1 = UIImage(named: "cloud1")
    
    static let cloud2 = UIImage(named: "cloud2")
}

class CloudAnimation: NSObject {
    
    var emitter = CAEmitterLayer()
    var images: [UIImage] = [Clouds.cloud1!, Clouds.cloud2!]
    
    var velocities:[Int] = [ 10,20,30,40]
    
    var positions = [CGFloat]()
    
    
    func cloudsAppear(view: UIView) {
        
        
        
        positions = [view.frame.height / 2, view.frame.height / 4]
        
        emitter.emitterPosition = CGPoint(x: 1000, y: positions[randomPosition()])
        emitter.emitterShape = CAEmitterLayerEmitterShape.line
        emitter.emitterSize = CGSize(width: view.frame.size.width, height: 2.0)
        emitter.emitterCells = generateEmitterCells()
        view.layer.insertSublayer(emitter, at: 1)
    }
    
    private func generateEmitterCells() -> [CAEmitterCell] {
        var cells:[CAEmitterCell] = [CAEmitterCell]()
        for index in 0..<4 {
            
            let cell = CAEmitterCell()
            
            cell.birthRate = 0.03
            cell.lifetime = 100.0
            cell.lifetimeRange = 1
            cell.velocity = CGFloat(getRandomVelocity())
            cell.velocityRange = 0
            cell.emissionLongitude = CGFloat(-Double.pi / 2)
            cell.emissionRange = 0.5
            cell.spinRange = 0
            
            cell.contents = getNextImage(i: index)
            cell.scaleRange = 0.25
            cell.scale = 0.1
            
            cells.append(cell)
            
        }
        
        return cells
        
    }
    
    private func randomPosition() -> Int {
        
        return Int(arc4random_uniform(2))
    }
    
    private func getRandomVelocity() -> Int {
        return velocities[getRandomNumber()]
    }
    
    private func getRandomNumber() -> Int {
        return Int(arc4random_uniform(4))
    }
    
    
    private func getNextImage(i:Int) -> CGImage {
        
        let image = images[i % 2].resizeImage(width: 1500)
        
        return image!.cgImage!
    }
    
    

}

extension UIImage {
    func resizeImage(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    func resizeImage(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        imageView.alpha = 0.5
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}
