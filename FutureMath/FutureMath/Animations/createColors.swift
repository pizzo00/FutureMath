//
//  createColors.swift
//  FutureMath
//
//  Created by Alberto Battiston on 11/11/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class createColors: NSObject {
    
    func addBackGround(view: UIView) {
        
        let imageName = "background"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        imageView.contentMode = .scaleAspectFill
        
        let w = view.frame.size.width
        let h = view.frame.size.height
        
        imageView.frame = CGRect(x: 0, y: 0, width:w, height: h)
        view.insertSubview(imageView, at: 0)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func green() -> UIColor {
        
        return hexStringToUIColor(hex: "32D74B")
        
    }
    
    func red() -> UIColor{
        
        return hexStringToUIColor(hex: "FF453A")
        
    }
    
    func orange() -> UIColor {
        
        return hexStringToUIColor(hex: "FC8835")
    }
    
    func buttonShadows(but: UIButton) {
        
        but.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        but.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        but.layer.shadowOpacity = 1.0
        but.layer.shadowRadius = 0.0
        but.layer.masksToBounds = false
    }

}
