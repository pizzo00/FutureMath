//
//  ViewController.swift
//  FutureMath
//
//  Created by Davide on 02/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet var views: [UIView]!
    
    @IBOutlet var labels: [UILabel]!
    
    @IBOutlet var buttons: [UIButton]!
    
    @IBOutlet weak var viewEsercizio: UIView!
    @IBOutlet weak var titoloEs: UILabel!
    
    @IBOutlet weak var inizia: ZFRippleButton!
    @IBOutlet weak var fumetto: UIImageView!
    @IBOutlet weak var mathBot: UIImageView!
    @IBOutlet weak var descrizione: UILabel!
    @IBOutlet weak var viewDescrizione: UIView!
    
    var tag = Int()
    
    var reference = String()
    
    var references: [String] = ["moltiplicazioni_inverse", "divisioni_inverse", "frazioni_complementari", "abaco", "moltiplicazioni", "divisori_di", "cent_dec_unita","confronto_numeri", "multipli_di", "divisioni", "amici_10", "risposta_multipla"]
    
    var blurEffectView = UIVisualEffectView()
    
   override func viewDidLoad() {
        super.viewDidLoad()
    
        self.mathBot.isUserInteractionEnabled = true
    
        for view in self.views {
        
            view.alpha = 0
        }
    
        
        self.view.backgroundColor = createColors().hexStringToUIColor(hex: "F9C437")
    
        self.viewEsercizioStyle()
        
    }
    
    private func blurEffect() {
        
        let blurEffect = UIBlurEffect(style: .regular)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        view.insertSubview(blurEffectView, belowSubview: self.viewEsercizio)
    }
    
    private func viewEsercizioStyle() {
        
        self.viewEsercizio.alpha = 0
    
        self.viewEsercizio.layer.cornerRadius = 20
        self.viewEsercizio.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.viewEsercizio.layer.shadowOffset = CGSize(width: 0.0, height: 8.0)
        self.viewEsercizio.layer.shadowOpacity = 1.0
        self.viewEsercizio.layer.shadowRadius = 0.0
        self.viewEsercizio.layer.masksToBounds = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
            UIView.animate(withDuration: 1.0, delay: 1.5, options: [], animations: {
                for view in self.views {
                    view.alpha = 1
                }
            }) { (true) in
                
                for but in self.buttons {
                    
                    self.floatingAnimation(but: but)
                }
            }
    }
    
    private func floatingAnimation(but: UIButton){
        UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat, .autoreverse, .allowUserInteraction], animations: {
            but.center.y -= 7
        }, completion: nil)
        
    }
    
    @IBAction func navAction(_ sender: UIButton) {
        
        tag = sender.tag + 20
        
        UIView.animate(withDuration: 0.4) {
            self.viewEsercizio.isHidden = false
            self.viewEsercizio.alpha = 1
            self.fumettoDescrizioneAnimation()
        }
        
        self.blurEffect()
        
        let labelFound = self.labels.filter{ $0.tag == tag }.first
        
        reference = self.references[sender.tag - 1]
        
        self.titoloEs.text = labelFound?.text
        
    }

    @IBAction func cambiaEs(_ sender: Any) {
        
        UIView.animate(withDuration: 0.4, animations: {
            self.viewEsercizio.alpha = 0
        }) { (true) in
            self.viewEsercizio.isHidden = true
            self.viewEsercizio.alpha = 1
            self.blurEffectView.removeFromSuperview()
            
        }
    }
    @IBAction func iniziaEs(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: reference)
        newViewController.transitioningDelegate = self
        newViewController.modalPresentationStyle = .custom
        self.present(newViewController, animated: true, completion: nil)
        
        self.viewEsercizio.isHidden = true
        self.blurEffectView.removeFromSuperview()
        
    }
    //tocca fuori dalla view per chiuderla
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = (touches.first)
    
        if touch?.view == self.blurEffectView {
            UIView.animate(withDuration: 0.4, animations: {
                self.viewEsercizio.alpha = 0
            }) { (true) in
                self.viewEsercizio.isHidden = true
                self.viewEsercizio.alpha = 1
                self.blurEffectView.removeFromSuperview()
            }
        }
    
    }
    
    private func mathBotAnimation(xPos: Int) {
        
        //posizione finale x= 48, y = 0
        UIView.animate(withDuration: 1.2, delay: 0.5, options: [.curveEaseOut], animations: {
            self.mathBot.frame.origin = CGPoint(x: xPos, y: 0)
        }, completion: nil)
    }
    
    private func fumettoDescrizioneAnimation() {
        
        UIView.animate(withDuration: 1.2, delay: 0.5, options: [.curveEaseOut], animations: {
            
            self.fumetto.isHidden = false
            self.descrizione.isHidden = false
     
        }, completion: nil)
    }
}

extension ViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FlipPresentAnimationController(originFrame: self.viewEsercizio.frame)
    }

}

