//
//  divisoriDi_ViewController.swift
//  FutureMath
//
//  Piccolo Marco & Peron Samuel, 5^BI
//  Created on 18/10/2018
//

import UIKit

class divisoriDi_ViewController: UIViewController {
    
    let confetti = confettiFalling()
    let color = createColors()

    var turn : Turn?
    var selectedNumbers = [Int]()               //numbers selected by the user
    
    @IBOutlet weak var lblMainNum: UILabel!     //displays the mainNum
    @IBOutlet weak var lblResult: UILabel!      //shows if the user has won or has to try again
    @IBOutlet weak var lblDividers: UILabel!    //displays the number of dividers that the mainNumhas by using "_" and shows the numbers selected by the user
 
    @IBOutlet weak var btnRetry: ZFRippleButton!
    @IBOutlet weak var btnNext: ZFRippleButton!
    @IBOutlet weak var viewBtn: UIView!
    @IBOutlet weak var back: ZFRippleButton!
    @IBOutlet var buttons: [UIButton]!
    
    @IBAction func btnDividers(_ sender: Any) {
        if selectedNumbers.count != turn?.dividers.count {
            changeButton(sender as! UIButton)       //changes the appearance of the selected button and selects the corresponding number
            displayDividersAndResult()              //updates lblDividers
            checkRightSelection()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnRetry(_ sender: Any) {
        resetButtons()
        selectedNumbers.removeAll()
        displayDividersAndResult()
        self.confetti.stopAnimation(view: self.view)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        btnNext.alpha = 0.5
        btnNext.isEnabled = false
        start()
        self.confetti.stopAnimation(view: self.view)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createColors().addBackGround(view: self.view)
        
        start()
        
        buttonShadows(but: back)
        buttonShadows(but: btnRetry)
        buttonShadows(but: btnNext)
        
        for but in buttons {
            buttonShadows(but: but)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Score.shared.registerStoryboard(storyboard: self)
    }
    
    private func buttonShadows(but: UIButton) {
        
        but.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        but.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        but.layer.shadowOpacity = 1.0
        but.layer.shadowRadius = 0.0
        but.layer.masksToBounds = false
    }
    
    //generates a new turn, and so a new mainNum, resets the buttons to a normal state and fills them with the new dividers
    private func start() {
        
        btnRetry.isEnabled = true
        btnRetry.alpha = 1
        turn = Turn()
        lblMainNum.text = String(turn!.mainNum)
        resetButtons()
        fillButtons()
        selectedNumbers.removeAll()
        displayDividersAndResult()
    }
    
    //randomly gives all buttons a number from the dividers or fakeDividers array
    private func fillButtons() {
        var numbers = turn!.dividers + turn!.fakeDividers
        numbers.shuffle()
        var i = 0
        
        for case let btn as UIButton in viewBtn.subviews {
            btn.setTitle(String(numbers[i]), for: UIControl.State.normal)
            i += 1
        }
    }
    
    //resets all buttons tags to 0 and title color to blue
    private func resetButtons() {
        for case let button as UIButton in viewBtn.subviews {
            button.tag = 0
            button.setTitleColor(UIColor.white, for: .normal)
        }
    }
    
    //toggles the selected button by looking at the button tag, adding or removing the button number from the selectedNumbers array
    private func changeButton(_ btn : UIButton) {
        btn.setTitleColor((btn.tag == 0) ? UIColor.darkGray : UIColor.white, for: .normal)
        
        if(btn.tag == 0) {
            selectedNumbers.append(Int(btn.title(for: .normal)!)!)
        } else {
            selectedNumbers.remove(at: selectedNumbers.firstIndex(of: Int(btn.title(for: UIControl.State.normal)!)!)!)
        }
        
        btn.tag = (btn.tag == 0) ? 1 : 0
    }
    
    //once the user has selected enough numbers, this method checks if he has won, if he has to retry or if he has lost
    private func checkRightSelection() {
        if checkSelectionIsFull() {
            if turn!.checkDividers(selectedNumbers) {
                selectionIsRight()
            } else {
                if Score.shared.wrongAnswer().hasLost {
                    loss()
                } else {
                    selectionIsWrong()
                }
            }
        }
    }
    
    private func selectionIsRight()
    {
        lblResult.text = "Bravo, è corretto!"
        lblResult.backgroundColor = createColors().green()
        self.confetti.startAnimation(view: self.view)
        displayMistakes(false)
        btnNext.isHidden = false
        btnNext.alpha = 1
        btnNext.isEnabled = true
        do {
            try _ = Score.shared.rightAnswer(pointsGained: (turn?.dividers.count)! - 1) //the score given is based on the number of dividers that the user had to find
        } catch {
            print("error")
        }
    }
    
    private func selectionIsWrong()
    {
        lblResult.text = "Ops, riprova!"
        
        lblResult.backgroundColor = createColors().red()
        displayMistakes(false)
        btnRetry.isHidden = false
    }
    
    private func loss()
    {
        lblResult.text = "Peccato..."
        lblResult.backgroundColor = createColors().red()
        displayMistakes(true)
        btnNext.isHidden = false
    }
    
    
    //returns a boolean number after checking if the number of selected numbers is equal to the number of dividers of the mainNum
    private func checkSelectionIsFull() -> Bool {
        return selectedNumbers.count == turn?.dividers.count
    }
    
    //creates a string that contains the numbers selected by the user and how many numbers he has to select, by using "_", and adds it to lblDividers.
    //It also sets hidden or not the label lblResult by checking if the selectedNumbers array is full
    private func displayDividersAndResult() {
        var div = ""
        
        for num in selectedNumbers {
            div.append(String(num))
            div.append("  ")
        }
        for _ in 0 ..< ((turn?.dividers.count)! - selectedNumbers.count) {
            div.append("_")
            div.append("  ")
        }
        
        lblDividers.text = div
        lblResult.isHidden = !checkSelectionIsFull()
    }
    
    //highlights which of the selected dividers are right or wrong by changing the button title color to red or green
    private func displayMistakes(_ isLoss : Bool) {
        for case let btn as UIButton in viewBtn.subviews {
            let num = Int(btn.title(for: UIControl.State.normal)!)!
            if (isLoss) ? true : (selectedNumbers.contains(num)) {
                btn.setTitleColor(((turn?.dividers.contains(num))!) ? UIColor.green : UIColor.red, for: UIControl.State.normal)
            } else {
                btn.setTitleColor(UIColor.white, for: .normal)
            }
        }
    }
}
