//
//  Turn.swift
//  FutureMath
//
//  Piccolo Marco & Peron Samuel, 5^BI
//  Created on 02/10/2018
//

import Foundation

public class Turn{
    
    var mainNum : Int           //number to which the user must find the dividers
    var dividers = [Int]()      //array containing all the dividers of the number
    var fakeDividers = [Int]()  //array containing random numbers that aren't dividers
    
    init(){
        mainNum = Int((arc4random() % 13) + 8)      //mainNum must be a random number
        findDividers()
        findFakeDividers()
    }
    
    //find all dividers of the mainNum
    private func findDividers(){
        dividers.removeAll()
        for i in 1...mainNum {
            if mainNum % i == 0 {
                dividers.append(i)
            }
        }
    }
    
    //generates random numbers that will be the "fake dividers"
    private func findFakeDividers(){
        fakeDividers.removeAll()
        while fakeDividers.count < 12 - dividers.count {
            
            //numbers must be different from each other and must not be too high from the mainNum
            var num = Int((arc4random() % 20) + 1)
            while checkNumIsInArray(num, dividers) || checkNumIsInArray(num, fakeDividers) || checkNumIsNotTooHigh(num) {
                num = Int((arc4random() % 20) + 1)
            }
            
            fakeDividers.append(num)
        }
    }
    
    //checks if the numbers in the array passed as a parameter are all dividers of the mainNum
    public func checkDividers(_ array : [Int]) -> Bool {
        var res = true;
        for i in array {
            if !dividers.contains(i) {
                res = false;
            }
        }
        return res
    }
    
    //checks if a number is contained in an array, both passed as parameters, and returns a bool value
    private func checkNumIsInArray(_ num : Int, _ arr : [Int]) -> Bool {
        return arr.contains(num)
    }
    
    //checks if the number passed as a parameter is too high or not from the mainNum, and return a bool value
    //if the number is smaller than 12, then the function checks if the given number is higher than mainNum + 4
    private func checkNumIsNotTooHigh(_ num : Int) -> Bool {
        if mainNum < 12 {
            return num > (mainNum + 4)
        } else {
            return num > mainNum
        }
    }
}
