//
//  frazioni.swift
//  FutureMath
//
//  Created by Studente on 16/10/2018.
//  Copyright © 2018 5BI. All rights reserved. 
//

import UIKit

class frazioni: UIViewController {
    
    @IBOutlet weak var num1: UILabel!
    @IBOutlet weak var den1: UILabel!
    @IBOutlet weak var num2: UITextField!
    @IBOutlet weak var den2: UILabel!
    @IBOutlet weak var numRis: UILabel!
    @IBOutlet weak var denRis: UILabel!
    
    @IBOutlet weak var winningLabel: UILabel!
    @IBOutlet weak var back: ZFRippleButton!
    
    @IBOutlet weak var retry: ZFRippleButton!
    
    @IBOutlet weak var controlla: ZFRippleButton!
    var numeratoreGiusto = 0
    
    let confetti = confettiFalling()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        num2.layer.borderWidth = 3
        num2.layer.borderColor = createColors().orange().cgColor
        num2.backgroundColor = .white
        num2.layer.cornerRadius = 15
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.backgroundColor = UIColor.lightGray
        metoDino()
        Score.shared.registerStoryboard(storyboard: self)
        createColors().addBackGround(view: self.view)
    }
    
    func metoDino() {
        let randomDenominatore = Int.random(in: 2 ..< 10)
        let randomNumber = Int.random(in: 1 ..< randomDenominatore)
        numRis.text = String(randomDenominatore)
        num1.text = String(randomNumber)
        numeratoreGiusto = (randomDenominatore-randomNumber)
        (den1.text, den2.text, denRis.text) = (String(randomDenominatore), String(randomDenominatore),String(randomDenominatore))
    }
    
    @IBAction func controllo(_ sender: Any) {
        self.winningLabel.isHidden = false
        if(num2.text == String(numeratoreGiusto))
        {
            self.winningLabel.text = "Bravo, è corretto!"
            self.winningLabel.backgroundColor = createColors().green()
            confetti.startAnimation(view: self.view)
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 1)
            }
            
            catch{
                print("ciao")
            }
        }
        else
        {
            self.winningLabel.text = "Ops, riprova!"
            self.winningLabel.backgroundColor = createColors().red()
            _ = Score.shared.wrongAnswer()
        }
        
    }
    @IBAction func back(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    //FATTO DA BATTISTON !!!!!!!!!!!!!!!!!!!!
    
    @IBAction func retry(_ sender: Any) {
        
        metoDino()
        self.num2 = nil
        self.winningLabel.isHidden = true
        confetti.stopAnimation(view: self.view)
    }
}
