//
//  AbacoRow.swift
//  FutureMath
//
//  Created by marco on 01/11/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class AbacoRow: UICollectionView {
    
    var balls = [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0]
    
    func addBall() {
        let zeroLastIndex = balls.lastIndex(of: 0)
        
        if (zeroLastIndex != 1) {
            let oneLastIndex = zeroLastIndex! - 2
            
            balls.swapAt(zeroLastIndex!, oneLastIndex)
            self.reloadData()
        }
    }
    
    func removeBall() {
        let zeroFirstIndex = balls.firstIndex(of: 0)
        
        if (zeroFirstIndex != 9) {
            let oneFirstIndex = zeroFirstIndex! + 2
            
            balls.swapAt(zeroFirstIndex!, oneFirstIndex)
            self.reloadData()
        }
    }
    
    func reset() {
        balls = [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0]
        self.reloadData()
    }
    
    func getValue() -> Int {
        let zeroLastIndex = balls.lastIndex(of: 0)
        let value = balls.count - zeroLastIndex! - 1
        return value
    }

}
