//
//  abaco.swift
//  FutureMath
//
//  Created by Davide on 17/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class abaco: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var winningLabel: UILabel!
    
    @IBOutlet var abacoRows: [AbacoRow]!
    
    @IBOutlet weak var replayBut: ZFRippleButton!
    @IBOutlet weak var backButton: ZFRippleButton!
    @IBOutlet weak var check: ZFRippleButton!
    
    var confetti = confettiFalling()
    var number: Number!
    var ballsName = ["biglia_blu.png", "biglia_verde.png", "biglia_rossa.png", "biglia_gialla.png"]
    
    /****************************************************************************************************/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createColors().addBackGround(view: self.view)
        
        
        buttonShadows(but: replayBut)
        buttonShadows(but: backButton)
        buttonShadows(but: check)
                
        number = Number()
        generateNumber()
        
        // add swipe recognizer to each row
        for row in abacoRows {
            let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(abaco.rightSwipe(sender:)))
            rightSwipe.direction = .right
            
            let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(abaco.leftSwipe(sender:)))
            leftSwipe.direction = .left
            
            row.addGestureRecognizer(rightSwipe)
            row.addGestureRecognizer(leftSwipe)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Score.shared.registerStoryboard(storyboard: self)
    }
    
    private func buttonShadows(but: UIButton) {
        
        but.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        but.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        but.layer.shadowOpacity = 1.0
        but.layer.shadowRadius = 0.0
        but.layer.masksToBounds = false
    }
    
    func generateNumber(){
        ballsName.shuffle()
        number.regenerate()
        // number.prettyPrint()
        numberLabel.text = String(number.number)
    }
    
    func checkWinning() {
        
        let a = Score.shared.wrongAnswer()
        
        if (abacoRows[0].getValue() == number.tens &&
            abacoRows[1].getValue() == number.units &&
            abacoRows[2].getValue() == number.tenths &&
            abacoRows[3].getValue() == number.cents) {
            
            try! _ = Score.shared.rightAnswer(pointsGained: 3)
            
            self.winningLabel.backgroundColor = createColors().green()
            self.winningLabel.text = "Bravo, hai vinto!"
            self.winningLabel.isHidden = false
            self.replayBut.isHidden = false
            
            confetti.startAnimation(view: self.view)
            
            if a.isNewRecord {
                
                self.winningLabel.backgroundColor = createColors().green()
                self.winningLabel.text = "Nuovo record!"
                UIView.animate(withDuration: 0.5) {
                    self.winningLabel.alpha = 1
                }
                self.replayBut.isEnabled = true
                confettiFalling().startAnimation(view: self.view)
            }
            
        }
        else {
            _ = Score.shared.wrongAnswer()
            self.winningLabel.backgroundColor = createColors().red()
            self.winningLabel.text = "Ops, è sbagliato"
            self.winningLabel.isHidden = false
            UIView.animate(withDuration: 3.0, animations: {
                self.winningLabel.alpha = 0
            }) { (true) in
                self.winningLabel.isHidden = true
                self.winningLabel.alpha = 1
            }
        }
    }
    
    func restart() {
        generateNumber()
        
        self.winningLabel.isHidden = true
        self.confetti.stopAnimation(view: self.view)
        
        for row in abacoRows {
            row.reset()
        }
    }
    
    /****************************************************************************************************/
    
    
    /* swipe events */
    
    @objc func rightSwipe(sender: UISwipeGestureRecognizer){
        (sender.view as! AbacoRow).addBall()
    }
    
    @objc func leftSwipe(sender: UISwipeGestureRecognizer){
        (sender.view as! AbacoRow).removeBall()
    }
    
    /****************************************************************************************************/
    
    
    /* buttons actions */
    
    @IBAction func restartTouched(_ sender: Any) {
        restart()
    }
    
    @IBAction func checkTouched(_ sender: Any) {
        checkWinning()
    }
    
    @IBAction func backBut(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    /****************************************************************************************************/
    
    
    /* rows initialization */
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 11   // balls in a row
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let ball = collectionView.dequeueReusableCell(withReuseIdentifier: "Ball", for: indexPath) as! AbacoBall
        
        let abacoRow = collectionView as! AbacoRow
        
        // blank image if value == 0, else ball image
        var image: UIImage? = nil
        if (abacoRow.balls[indexPath.item] == 1) {
            let name = ballsName[abacoRow.tag]
            image = UIImage(named: name)
        }
        
        ball.ballImage.image = image
        
        return ball
    }

}
