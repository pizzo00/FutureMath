//
//  Number.swift
//  FutureMath
//
//  Created by Marco on 23/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import Foundation

class Number {
    var number: Float
    
    var tens = 0, units = 0, tenths = 0, cents = 0
    
    // unit object using given number
    init(number: Float){
        self.number = number
        extractValues()
    }
    
    // init object with random number
    init(){
        self.number = Float(String(format: "%.2f", Float.random(in: 0...99)))!
        extractValues()
    }
    
    // regenerates object with random number
    func regenerate(){
        self.number = Float(String(format: "%.2f", Float.random(in: 0...99)))!
        extractValues()
    }
    
    // pretty print the number values in a very pretty way
    func prettyPrint(){
        print("Number: \(self.number), Tens: \(self.tens), Units: \(self.units), Tenths: \(self.tenths), Cents: \(self.cents)")
    }
    
    // get tens, units... from starting number
    private func extractValues(){
        self.tens = Int((self.number / 10).truncatingRemainder(dividingBy: 10))
        self.units = Int(self.number.truncatingRemainder(dividingBy: 10))
        self.tenths = Int((self.number * 10).truncatingRemainder(dividingBy: 10))
        self.cents = Int(round(self.number * 100).truncatingRemainder(dividingBy: 10))
    }
}

