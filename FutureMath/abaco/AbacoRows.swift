//
//  AbacoRows.swift
//  FutureMath
//
//  Created by marco on 01/11/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class AbacoRows: NSObject, UICollectionViewDataSource, UICollectionViewDelegate{
    
    @IBOutlet weak var abacoRowTens: UICollectionView!
    @IBOutlet weak var abacoRowUnits: UICollectionView!
    @IBOutlet weak var abacoRowTeths: UICollectionView!
    @IBOutlet weak var abacoRowCents: UICollectionView!
    
    // returns the number of balls that a row should display
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9    }
    
    // returns each ball image
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let ball = collectionView.dequeueReusableCell(withReuseIdentifier: "Ball", for: indexPath) as! AbacoBall
        
        ball.ballImage.image = UIImage(named: "biglia_verde.png")
        
        return ball
    }
}
