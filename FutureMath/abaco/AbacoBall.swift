//
//  AbacoBall.swift
//  FutureMath
//
//  Created by marco on 01/11/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class AbacoBall: UICollectionViewCell {
    
    @IBOutlet weak var ballImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    public func configure(ball: UIImage) {
        ballImage.image = ball
    }
}
