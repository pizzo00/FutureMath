//
//  cent_dec_unit.swift
//  FutureMath
//
//  Created by Studente on 16/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class cent_dec_unit: UIViewController {
    
    //dichiarazione bottoni numeri da inserire
    @IBOutlet weak var testoNumeroRandom: UILabel!
    @IBOutlet var numeri: [UIButton]!
    
    //dichiarazione bottoni testo
    @IBOutlet weak var testoCentinaia: UIButton!
    @IBOutlet weak var testoDecine: UIButton!
    @IBOutlet weak var testoUnita: UIButton!
    @IBOutlet weak var testoDecimi: UIButton!
    @IBOutlet weak var testoCentesimi: UIButton!

    //label di controllo
    @IBOutlet weak var correttoSbagliato: UILabel!
    
    //dichiarazione label bottoni testo
    @IBOutlet weak var labelCentinaia: UILabel!
    @IBOutlet weak var labelDecine: UILabel!
    @IBOutlet weak var labelUnita: UILabel!
    @IBOutlet weak var labelDecimi: UILabel!
    @IBOutlet weak var labelCentesimi: UILabel!
    
    @IBOutlet weak var back: ZFRippleButton!
    @IBOutlet weak var retry: ZFRippleButton!
    @IBOutlet weak var controlla: ZFRippleButton!
    
    @IBOutlet var bottoni: [UIButton]!
    
    //dichiarazione variabili
    var scelta = ""
    var numeroRandom: Float = 0
    var centinaia = 0
    var decine = 0
    var unita = 0
    var decimi = 0
    var centesimi = 0
    
    var centinaiaNumeroRandom = 0
    var decineNumeroRandom = 0
    var unitaNumeroRandom = 0
    var decimiNumeroRandom = 0
    var centesimiNumeroRandom = 0
    
    //dichiarazione array per swappare tra loro l'ordine delle cifre
    var xPosizioni = [87,222, 357,492,627,0 ]
    
    var ordineBottoni:[UIButton] = []
    var ordineLabel:[UILabel] = []

    //dichiarazione booleana
    var mosso = false
    var mossoGiu = false
    
    let confetti = confettiFalling()
    
    var selected = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let colors = createColors()
        
        colors.buttonShadows(but: self.controlla)
        colors.buttonShadows(but: self.back)
        colors.buttonShadows(but: self.retry)
        
        for numero in numeri {
            
            colors.buttonShadows(but: numero)
        }
        
        colors.buttonShadows(but: self.testoUnita)
        colors.buttonShadows(but: self.testoDecimi)
        colors.buttonShadows(but: self.testoDecine)
        colors.buttonShadows(but: self.testoCentesimi)
        colors.buttonShadows(but: self.testoCentinaia)
        
        createColors().addBackGround(view: self.view)
        ordineBottoni = [testoCentinaia, testoDecine, testoUnita, testoDecimi, testoCentesimi]
        ordineLabel = [labelCentinaia, labelDecine, labelUnita, labelDecimi, labelCentesimi]
        
        assegnamentoNumeroRandom()
        swappamento()
        riposizionamentoNumeri()
        
        disabledStyle(but: self.controlla)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Score.shared.registerStoryboard(storyboard: self)
        
        
    }
    
    private func disabledStyle(but: UIButton) {
       
        but.alpha = 0.5
        but.isEnabled = false
    }
    private func enabledStyle(but: UIButton) {
        
        but.alpha = 1
        but.isEnabled = true
    }

    //ricomincia una partita (sempre visibile)
    @IBAction func rigioca(_ sender: UIButton) {
        mosso = false
        azzeraValori()
        riposizionamentoNumeri()
        sbloccaBottoni()
        rimuoviSelezione()
        confetti.stopAnimation(view: self.view)
        
        enabledStyle(but: self.controlla)
        
    }
    
    @IBAction func back(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func numero(_ sender: UIButton) {
        
        correttoSbagliato.isHidden = true
        for but in self.bottoni {
            but.layer.borderWidth = 0
            but.layer.borderColor = nil
            
        }
        enabledStyle(but: self.controlla)
        
        switch scelta{
        case "testoCentinaia":
            testoCentinaia.setTitle(String(sender.tag), for: .normal)
            centinaia = sender.tag
        case "testoDecine":
            testoDecine.setTitle(String(sender.tag), for: .normal)
            decine = sender.tag
        case "testoUnita":
            testoUnita.setTitle(String(sender.tag), for: .normal)
            unita  = sender.tag
        case "testoDecimi":
            testoDecimi.setTitle(String(sender.tag), for: .normal)
            decimi = sender.tag
        case "testoCentesimi":
            testoCentesimi.setTitle(String(sender.tag), for: .normal)
            centesimi  = sender.tag
        default:
            scelta = ""
        }
        riposizionamentoNumeri()
        mosso = false
        rimuoviSelezione()
        
        
        
    }

    //in base al bottone cliccato, i numeri vengono inseriti in un certo bottone ed una carta variabile
    @IBAction func sceltaPosizione(_ sender: UIButton) {
        
        
        
        for but in bottoni {
            but.layer.borderWidth = 0
            but.layer.borderColor = nil
            
        }

        rimuoviSelezione()
        comparsaNumeri()
        
        correttoSbagliato.isHidden = true
        
        sender.layer.borderWidth = 5
        sender.layer.borderColor = UIColor.red.cgColor

        switch sender.tag{
        case 100:
            scelta = "testoCentinaia"
        case 10:
            scelta = "testoDecine"
        case 1:
            scelta = "testoUnita"
        case -10:
            scelta = "testoDecimi"
        case -100:
            scelta = "testoCentesimi"
        default:
            scelta = ""
        }
    }

    
    // controlla che tutti i valori delle cifre siano corretti e di conseguenza indica la vittoria o l'errore (in questo caso chiede di riprovare finchè non si fa corretto)
    @IBAction func metodoControllo(_ sender: UIButton) {
            correttoSbagliato.isHidden = false
        
            disabledStyle(but: sender)
        if((centinaiaNumeroRandom==centinaia)&&(decineNumeroRandom==decine)&&(unitaNumeroRandom==unita)&&(decimiNumeroRandom==decimi)&&(centesimiNumeroRandom==centesimi)){
                correttoSbagliato.text = "Bravo, è corretto!"
                correttoSbagliato.backgroundColor = createColors().green()
        
                confetti.startAnimation(view: self.view)
                do
                {
                    try _ = Score.shared.rightAnswer(pointsGained: 1)
                }
                catch
                {
                    print("Error")
                }
                bloccaBottoni()
                self.retry.isHidden = false
            }
            else{
                correttoSbagliato.text = "Ops, è sbagliato"
                correttoSbagliato.backgroundColor = createColors().red()
                _ = Score.shared.wrongAnswer()
            }
    }
    
    // disabilita tutti i bottoni tranne il rigioca (quando si vince)
    
    func bloccaBottoni(){
        for numero in numeri{
            numero.isEnabled = false
        }
        testoCentinaia.isEnabled = false
        testoDecine.isEnabled = false
        testoUnita.isEnabled = false
        testoDecimi.isEnabled = false
        testoCentesimi.isEnabled = false
    }
    
    //genera il numero casuale
    
    func assegnamentoNumeroRandom(){
        numeroRandom = Float.random(in: 0..<1000)
        numeroRandom = (numeroRandom * 100).rounded() / 100
        testoNumeroRandom.text = String(numeroRandom)
        suddivisioneNumeroRandom()
        
    }
    
    // riporta a 0 il valore dei bottoni di scelta
    
    func azzeraValori(){
        swappamento()
        testoCentinaia.setTitle(String(""), for: .normal)
        centinaia = 0
        testoDecine.setTitle(String(""), for: .normal)
        decine = 0
        testoUnita.setTitle(String(""), for: .normal)
        unita = 0
        testoDecimi.setTitle(String(""), for: .normal)
        decimi = 0
        testoCentesimi.setTitle(String(""), for: .normal)
        centesimi = 0
        correttoSbagliato.isHidden = true
        assegnamentoNumeroRandom()
        
    }
    
    
    // separe le cifre del numero random generato per centinaia, decine, unità, decimi e centesimi
    
    func suddivisioneNumeroRandom(){
        centinaiaNumeroRandom = Int((numeroRandom / 100).truncatingRemainder(dividingBy: 10))
        decineNumeroRandom = Int((numeroRandom / 10).truncatingRemainder(dividingBy: 10))
        unitaNumeroRandom = Int((numeroRandom / 1).truncatingRemainder(dividingBy: 10))
        decimiNumeroRandom = Int((numeroRandom * 10).truncatingRemainder(dividingBy: 10))
        centesimiNumeroRandom = Int((numeroRandom * 100).truncatingRemainder(dividingBy: 10))
        
        
        /*PER TESTARE IL CORRETTO SEPARAMENTO DELLE CIFRE---- FUNZIONANTE!
        testoCentinaia.setTitle(String(centinaiaNumeroRandom), for: .normal)
        testoDecine.setTitle(String(decineNumeroRandom), for: .normal)
        testoUnita.setTitle(String(unitaNumeroRandom), for: .normal)
        testoDecimi.setTitle(String(decimiNumeroRandom), for: .normal)
        testoCentesimi.setTitle(String(centesimiNumeroRandom), for: .normal)
        */
    }
    
    //sblocca i bottoni
    
    func sbloccaBottoni(){
        for numero in numeri{
            numero.isEnabled = true
        }
        
        testoCentinaia.isEnabled = true
        testoDecine.isEnabled = true
        testoUnita.isEnabled = true
        testoDecimi.isEnabled = true
        testoCentesimi.isEnabled = true
        
    }
    
    //posiziona l'ordine dei pulsanti di scelta in modo casuale
    func swappamento(){
        for i in 0...4{ //scambia le posizioni dei valori X delle label nell'array in modo casuale
            let numeroRandomLocale = Int.random(in: 0..<5)
            xPosizioni[5] = xPosizioni[i]
            xPosizioni[i] = xPosizioni[numeroRandomLocale]
            xPosizioni[numeroRandomLocale] = xPosizioni[5]
        }
        
        //assegna il valore X della posizione ai bottoni ed alle label.
        
        testoCentinaia.frame.origin = CGPoint(x: xPosizioni[0] + 20, y:470)
        testoDecine.frame.origin = CGPoint(x: xPosizioni[1] + 20, y:470)
        testoUnita.frame.origin = CGPoint(x: xPosizioni[2] + 20, y:470)
        testoDecimi.frame.origin = CGPoint(x: xPosizioni[3] + 20, y:470)
        testoCentesimi.frame.origin = CGPoint(x: xPosizioni[4] + 20, y:470)
        
        labelCentinaia.frame.origin = CGPoint(x: xPosizioni[0], y:400)
        labelDecine.frame.origin = CGPoint(x: xPosizioni[1], y:400)
        labelUnita.frame.origin = CGPoint(x: xPosizioni[2], y:400)
        labelDecimi.frame.origin = CGPoint(x: xPosizioni[3], y:400)
        labelCentesimi.frame.origin = CGPoint(x: xPosizioni[4], y:400)
    }
    
    
    
    //animazione per far apparire i numeri da selezionare
    func comparsaNumeri(){
        
        self.controlla.isHidden = true
        self.back.isHidden = true
        self.retry.isHidden = true
        if(mosso == false){
        UIView.animate(withDuration: 0.5, animations: {
            for numero in self.numeri {
                numero.frame.origin.y -= 250}
        }, completion: nil )
        }
        mosso = true
        mossoGiu = false
    }
    
    func riposizionamentoNumeri(){
        
        if (mossoGiu == false){
        UIView.animate(withDuration: 0.5, animations: {
            for numero in self.numeri {
                numero.frame.origin.y += 250}
        }, completion: {_ in
            self.controlla.isHidden = false
            self.back.isHidden = false
            self.retry.isHidden = false
            
        } )
        }
        mossoGiu = true
        
        
    }
    
    func rimuoviSelezione(){
        testoDecine.backgroundColor = createColors().orange()
        testoCentinaia.backgroundColor = createColors().orange()
        testoUnita.backgroundColor = createColors().orange()
        testoDecimi.backgroundColor = createColors().orange()
        testoCentesimi.backgroundColor = createColors().orange()
    }
}
