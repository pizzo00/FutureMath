//
//  ButtonStyle.swift
//  FutureMath
//
//  Created by Alberto Battiston on 31/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

@IBDesignable

class ButtonStyle: UIButton {
    
    @IBInspectable var cornerRadius: Float = 0 {
        
        didSet {
            
            layer.cornerRadius = CGFloat(cornerRadius)
            
        }
        
    }

    

}
