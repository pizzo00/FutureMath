//
//  multipli_di.swift
//  FutureMath
//
//  Created by Matteo Danieli on 31/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class multipli_di: UIViewController {
    
    @IBOutlet weak var NumeroM: UILabel!

    
    
    @IBOutlet weak var verifica: ZFRippleButton!
    
    @IBOutlet weak var verificaLabel: UILabel!
    
    @IBOutlet weak var multipli: UILabel!
    
    @IBOutlet var bottoni: [UIButton]!
    
    
    @IBOutlet weak var retry: ZFRippleButton!
    @IBOutlet weak var back: ZFRippleButton!
    
    var vittoria = true
    var indexstring = 0
    
    var multipliStringhe: [String] = [""," ",""," ",""," ",""]
    var partitella : partita!
    
    let colors = createColors()
    let confetti = confettiFalling()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        startGame()
        
        colors.addBackGround(view: self.view)
        colors.buttonShadows(but: self.verifica)
        colors.buttonShadows(but: self.retry)
        colors.buttonShadows(but: self.back)
        for bottone in bottoni {
            
            colors.buttonShadows(but: bottone)
            
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        Score.shared.registerStoryboard(storyboard: self)
    }
    
    private func enabledStyle(but: UIButton) {
        
        but.isEnabled = true
        but.alpha = 1
        
    }
    
    private func disabledStyle(but: UIButton) {
        
        but.isEnabled = false
        but.alpha = 0.5
        
        
    }
    
    private func startGame() {
        partitella = partita()
        partitella.IniziaPartita()
        riempiBottoni()
        NumeroM.text = String(partitella.numeroMain)
        self.multipli.text = ""
        verificaLabel.text = ""
        disabledStyle(but: self.verifica)
        
    }
    private func riempiBottoni(){
        for but in bottoni {
            but.setTitle(String(partitella.arrayTutti[(but.tag)-1]), for: .normal)
        }
    }
    
    @IBAction func back(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAction(_ sender: UIButton) {
        let numeroBut = Int((sender.titleLabel?.text!)!)
        let indexnum = partitella.RiempiSoluzioni(numeroBut: numeroBut!)
        sender.isEnabled = false

        if (indexnum>=4){
            for but in bottoni {
                disabledStyle(but: but)
            }
            enabledStyle(but: self.verifica)
        }
        
        let titoloBut = sender.titleLabel?.text
        multipliStringhe[indexstring] = titoloBut!
        RiscriviLabel()
    }
    private func RiscriviLabel()
    {
        for i in 0...3{
            multipli.text = multipli.text! + multipliStringhe[i]
        }
    }
    @IBAction func verifica(_ sender: UIButton) {
        
        verificaLabel.isHidden = false
        if(partitella.check()){
        verificaLabel.text = "Bravo, è corretto!"
        verificaLabel.backgroundColor = colors.green()
        confetti.startAnimation(view: self.view)
            disabledStyle(but: self.verifica)
            
            do {
                try _ = Score.shared.rightAnswer(pointsGained: 1)
                
            } catch {
                print("error")
            }
        
        }
        else {
            disabledStyle(but: self.verifica)
            
            if Score.shared.wrongAnswer().hasLost  {
                
                verificaLabel.text = "Hai perso tutte le vite"
                verificaLabel.backgroundColor = colors.red()
                
            } else {
                
                verificaLabel.text = "Ops, riprova!"
                verificaLabel.backgroundColor = colors.red()
            }
        }
    }
    @IBAction func replay(_ sender: Any) {
        for but in bottoni {
            enabledStyle(but: but)
        }
        startGame()
        
        confetti.stopAnimation(view: self.view)
        verificaLabel.isHidden = true
    }
    

    

}
