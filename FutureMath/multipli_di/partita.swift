//
//  partita.swift
//  FutureMath
//
//  Created by Matteo Danieli on 14/11/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import Foundation
class partita{
    var numeroMain = 0
    var arrayNonMultipli = [Int]()
    var arrayMultipli = [Int]()
    var arrayTutti = [Int]()
    var arraySoluzioni = [Int?](repeating: nil, count: 4)
    var arrayRandom = [Int]()
    var arrayNumeri = [Int]()
    
    var vittoria = true
    
    var indexnum = 0
    var indexstring = 0
    
    
    func IniziaPartita(){
        numeroMain = Int(arc4random() % 10 + 2 )
        riempiArray()
    }
    func RiempiSoluzioni(numeroBut: Int) -> Int{
        arraySoluzioni[indexnum] = numeroBut
        indexnum += 1
        return indexnum
        
    }
    func riempiArray(){
        riempiRandom()
        for i in 0...3{
            arrayMultipli.append(numeroMain*(arrayRandom[i]))
        }
        for _ in 0...3{
            var i = 1
            while (i == 1){
                var tempGrande = 0
                tempGrande = Int(arc4random() % 40 + 2)
                if(tempGrande%numeroMain != 0)
                {
                    arrayNonMultipli.append(tempGrande)
                    i = 0
                }
            }
            
        }
        arrayTutti = arrayNonMultipli+arrayMultipli
        arrayTutti.shuffle()
    }
    func check() -> Bool{
        for i in 0...3{
            if(arraySoluzioni[i]!%numeroMain != 0){
                vittoria = false
            }
        }
        return vittoria
    }
    private func riempiRandom(){//crea un array content
        for i in 0...20{
            arrayNumeri.append(i+1)
        }
        for _ in 0...3{
            let temp = Int(arc4random() % 10 + 2)
            arrayRandom.append(arrayNumeri[temp])
            arrayNumeri.remove(at: arrayNumeri[temp])

        }
        
        
    }
}
/*for _ in 0...3{
    var y = 0
    var passato = true
    while y == 0{
        let temp = Int(arc4random() % 10 + 1)
        for i in 0...3{
            if (temp == arrayRandom[i]){
                passato = false
            }
        }
        if(passato){
            arrayRandom.append(temp)
            y = 1
        }
    }
}*/
