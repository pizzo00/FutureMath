//
//  classiMoltiplicazioni.swift
//  FutureMath
//
//  Created by Davide on 10/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class classiMoltiplicazioni: NSObject {

    
    static let shared = classiMoltiplicazioni()
    
    var numeri = [Int]()
    let tot = 9
    var risultati = [Int]()
    var moltiplicatori = [Int]()
    
    
    func inizioPartita()     //resetta moltiplicatori e numeri(al momento calcola subito anche i risultati)
    {
        numeri = [Int]()
        risultati = [Int]()
        moltiplicatori = [Int]()
        var number : Int
        
        for _ in 1...2
        {
            number = Int.random(in: 1 ... 3)
            moltiplicatori.append(Int(pow(10.0 , Double(number))))
        }
        
        for _ in 0...tot{
            //numeri.append(roundf(( Float.random(in: 1.000 ... 100.000) * 100.00))/100.00)
            numeri.append(Int(arc4random() % 9000) + 1000)
        }
        //--metodo--
        for i in 0...tot{
            if (i < 5)
            {
            risultati.append(numeri[i]*moltiplicatori[0])
            }
            else
            {
                risultati.append((numeri[i]*moltiplicatori[1]))
            }
        }
    }
    
    func controllo(risposte : [Int]) -> [Bool]  //controlla le risposte giuste. Ritorna un array che indica quali giuste(true) e quali no(false)
    {
        var check = [Bool]()
        for i in 0...tot{
           
            if (risposte[i] ==
                risultati[i])
            
            {
                
                check.append(true)
            }
            else
            {
                check.append(false)
            }
        }
	        return check
        
    }
}
