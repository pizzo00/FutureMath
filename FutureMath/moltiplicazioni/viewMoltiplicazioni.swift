//
//  viewMoltiplicazioni.swift
//  FutureMath
//
//  Created by Davide on 09/10/2018.
//  Copyright © 2018 5BI. All rights reserved.
//

import UIKit

class viewMoltiplicazioni: UIViewController {
    
    
    //Button
    
    @IBOutlet weak var btnNuovaPartita: ZFRippleButton!
    @IBOutlet weak var controllo: ZFRippleButton!
    @IBOutlet weak var back: ZFRippleButton!
    //--Moltiplicatori--
    
    @IBOutlet weak var lblMoltiplicatore1: UILabel!
    @IBOutlet weak var lblMoltiplicatore2: UILabel!
    @IBOutlet weak var winningLabel: UILabel!
    
    //--Labels Valori Colonne--
    

    @IBOutlet var lblvalColl: [UILabel]!
    
    
    //--Button Valori Risposte--
    
    @IBOutlet var btnRisposte: [UIButton]!
    
    //--Button Tastierino
    
    @IBOutlet var btnTastierino: [UIButton]!
    
    //
    var casellaSelezionata = -1
    //
    
    let confetti = confettiFalling()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createColors().addBackGround(view: self.view)
        
        nuovaPartita()
        
        for button in btnRisposte {
            button.layer.borderWidth = 3
            button.layer.borderColor = createColors().orange().cgColor
            button.layer.cornerRadius = 15
            
        }
    

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Score.shared.registerStoryboard(storyboard: self)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func back(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    //--Button Controllo--
    @IBAction func btnCorrezione(_ sender: Any) {
        print("controllo")
        var numeri = [Int]()
        for i in 0...classiMoltiplicazioni.shared.tot
        {
            //numeri[i] = Float(txtRisposte[i].text)
            if (btnRisposte[i].currentTitle != nil && btnRisposte[i].currentTitle != "")
            {
                let num = Float(btnRisposte[i].currentTitle!)! * 100
                numeri.append(Int(num))
                print(numeri[i])
            }
            else
            {
                numeri.append(0)
            }
        }
        //classiMoltiplicazioni.shared.controllo(risposte: numeri)
        let corretto = classiMoltiplicazioni.shared.controllo(risposte: numeri)
        correzione(risultati: corretto)
        btnNuovaPartita.isHidden = false
        controllo.isHidden = true
    }
    
    @IBAction func btnNuovaPartitaAction(_ sender: Any) {
        nuovaPartita()
        sbloccoTastierino()
        btnNuovaPartita.isHidden = true
        controllo.isHidden = false
        btnRisposte[casellaSelezionata].layer.borderColor = createColors().orange().cgColor
        casellaSelezionata = -1
        for i in 0...classiMoltiplicazioni.shared.tot
        {
            btnRisposte[i].setTitle("", for: .normal)
            btnRisposte[i].backgroundColor = UIColor.clear
        }
        self.winningLabel.isHidden = true
    }
    
    func nuovaPartita()
    {
        classiMoltiplicazioni.shared.inizioPartita()
        lblMoltiplicatore1.text = String(classiMoltiplicazioni.shared.moltiplicatori[0])
        lblMoltiplicatore2.text = String(classiMoltiplicazioni.shared.moltiplicatori[1])
        var numeri = classiMoltiplicazioni.shared.numeri
        for i in 0...classiMoltiplicazioni.shared.tot
        {
            print(String(Float(numeri[i]).rounded()/100))
            lblvalColl[i].text = String(Float(numeri[i]).rounded()/100)
        }
        confetti.stopAnimation(view: self.view)
    }
    
    func correzione(risultati : [Bool])
    {
        self.winningLabel.isHidden = false
        bloccoTastierino()
        for i in 0...classiMoltiplicazioni.shared.tot
        {
            if (risultati[i]==true)
            {
                btnRisposte[i].backgroundColor = createColors().green()
            }
            else{
                btnRisposte[i].backgroundColor = createColors().red()
            }
        }
        if risultati.contains(false)
        {
            self.winningLabel.text = "Ops, riprova"
            self.winningLabel.backgroundColor = createColors().red()
            _ = Score.shared.wrongAnswer()
        }
        else
        {
            confetti.startAnimation(view: self.view)
            self.winningLabel.text = "Bravo, è corretto!"
            self.winningLabel.backgroundColor = createColors().green()
            do{
                try _ = Score.shared.rightAnswer(pointsGained: 10)
            }
            catch{
                print("ciao")
            }
            
        }
    }
    
    
    func bloccoTastierino()
    {
        for i in 0...11
        {
            btnTastierino[i].isHidden = true
        }
    }
    
    func sbloccoTastierino()
    {
        for i in 0...11
        {
            btnTastierino[i].isHidden = false
        }
        
    }
    
    
    @IBAction func selezionaCasella(_ sender: UIButton) {
        if (casellaSelezionata != -1)
        {
            btnRisposte[casellaSelezionata].layer.borderColor = createColors().orange().cgColor
        }
        casellaSelezionata = sender.tag
        btnRisposte[casellaSelezionata].layer.borderColor = createColors().green().cgColor
        print(casellaSelezionata)
    }
    
    @IBAction func tastierino(_ sender: UIButton) {
        if (casellaSelezionata != -1)
        {
            if (sender.tag > -1 && sender.tag < 10)
            {
                
                if (btnRisposte[casellaSelezionata].currentTitle != nil)
                {
                    if ((btnRisposte[casellaSelezionata].currentTitle?.count)! < 9)
                    {
                        btnRisposte[casellaSelezionata].setTitle(btnRisposte[casellaSelezionata].currentTitle! + String(sender.tag), for: .normal)
                    }
                }
                else{
                    btnRisposte[casellaSelezionata].setTitle(String(sender.tag), for: .normal)
                }
                
            }
            else if (sender.tag == 10)
            {
                
                    if (btnRisposte[casellaSelezionata].currentTitle != nil)
                    {
                        if ((btnRisposte[casellaSelezionata].currentTitle?.count)! < 9  && ((btnRisposte[casellaSelezionata].currentTitle?.components(separatedBy: ".").count)!-1 == 0))
                        {
                            btnRisposte[casellaSelezionata].setTitle(btnRisposte[casellaSelezionata].currentTitle! + ".", for: .normal)
                        }
                    }
                    else{
                        btnRisposte[casellaSelezionata].setTitle(".", for: .normal)
                    }
                
                
            }
            else if (sender.tag == 11)
            {
                btnRisposte[casellaSelezionata].setTitle("", for: .normal)
                
            }
        }
    }
    

}
